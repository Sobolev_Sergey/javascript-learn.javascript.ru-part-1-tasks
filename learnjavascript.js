/* 
ПРИВЕТ, МИР!

Выведите alert
Сделайте страницу, которая выводит «Я – JavaScript!».
Создайте её на диске, откройте в браузере, убедитесь, что всё работает.
*/

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
</head>

<body>

  <script>
    alert( 'Я - JavaScript!' );
  </script>

</body>

</html>


/*
ВНЕШНИЕ СКРИПТЫ, ПОРЯДОК ИСПОЛНЕНИЯ

Вывести alert внешним скриптом
Возьмите решение предыдущей задачи Выведите alert и вынесите скрипт во внешний 
файл alert.js, который расположите в той же директории.
Откройте страницу и проверьте, что вывод сообщения всё ещё работает.
*/

//index.html
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
</head>

<body>

  <script src="alert.js"></script>

</body>

</html>

//alert.js
alert( 'Я - JavaScript!' );


/*
Какой скрипт выполнится первым?
В примере ниже подключены два скрипта small.js и big.js.
Если предположить, что small.js загружается гораздо быстрее, 
чем big.js – какой выполнится первым?
*/
<script src="big.js"></script>				// 1
<script src="small.js"></script>			// 2
//А вот так?

<script async src="big.js"></script>		// 2
<script async src="small.js"></script>		// 1
//А так?

<script defer src="big.js"></script>		// 1
<script defer src="small.js"></script>		// 2



/*
ПЕРЕМЕННЫЕ

Работа с переменными
Объявите две переменные: admin и name.
Запишите в name строку "Василий".
Скопируйте значение из name в admin.
Выведите admin (должно вывести Василий).
*/

var admin, name;
name = "Василий";
admin = name;
alert(admin);


/*
ПРАВИЛЬНЫЙ ВЫБОР ИМЕНИ ПЕРЕМЕННОЙ

Объявление переменных
Создайте переменную для названия нашей планеты и присвойте ей значение "Земля". 
Правильное имя выберите сами.
Создайте переменную для имени посетителя со значением "Петя". 
Имя также на ваш вкус.
*/

var ourPlanetName = "Земля";
var userName = "Петя";


/*
ОСНОВНЫЕ ОПЕРАТОРЫ

Инкремент, порядок срабатывания
важность: 5
Посмотрите, понятно ли вам, почему код ниже работает именно так?
*/
 var a = 1, b = 1, c, d;

c = ++a; alert(c); // 2			// c=1+1=2 a=2
d = b++; alert(d); // 1			// d=1 b=1+1=2

c = (2+ ++a); alert(c); // 5	// a=2 c=(2+(2+1))=5 a=3
d = (2+ b++); alert(d); // 4	// b=2 d=(2+2)=4 b=2+1=3

alert(a); // 3
alert(b); // 3



/*
Результат присваивания

Чему будет равен x в примере ниже?
*/
var a = 2;
var x = 1 + (a *= 2);	// x=1+(a=2*2=4)=1+4=5



/*
Побитовые операторы

Проверка, целое ли число
Напишите функцию isInteger(num), которая возвращает true, 
если num – целое число, иначе false.
Например:
*/
alert( isInteger(1) ); 		// true
alert( isInteger(1.5) ); 	// false
alert( isInteger(-0.5) ); 	// false


function isInteger(num) {
  return (num ^ 0) === num;
}


/*
Взаимодействие с пользователем: alert, prompt, confirm

Простая страница
Создайте страницу, которая спрашивает имя и выводит его.
*/

var name = prompt('What is your name?', 'Name');
alert('Your name is ' + name)


/*
Условные операторы: if, '?'

if (строка с нулём)
Выведется ли alert?
*/
if ("0") {
  alert( 'Привет' ); 	// да, т.к. "0" - это строка и true
}


/*
Используя конструкцию if..else, напишите код, который будет спрашивать: 
«Каково «официальное» название JavaScript?».
Если посетитель вводит «ECMAScript», то выводить «Верно!», 
если что-то другое – выводить «Не знаете? «ECMAScript»!».
*/

var answer = prompt('Каково "официальное" название JavaScript?');
if (answer === "ECMAScript") {
  alert('Верно!');
} else {
  alert('Не знаете? "ECMAScript"!');
}


/*
Получить знак числа

Используя конструкцию if..else, напишите код, который получает значение prompt, 
а затем выводит alert:

1, если значение больше нуля,
-1, если значение меньше нуля,
0, если значение равно нулю.
*/

var num=prompt('Введите число ',0);
if (num > 0){
  alert(1);
} else if (num < 0) {
  alert(-1);
} else {
  alert(0);
}


/*
Проверка логина

Напишите код, который будет спрашивать логин (prompt).
Если посетитель вводит «Админ», то спрашивать пароль, 
если нажал отмена (escape) – выводить 
«Вход отменён», если вводит что-то другое – «Я вас не знаю».
Пароль проверять так. Если введён пароль «Чёрный Властелин», 
то выводить «Добро пожаловать!», 
иначе – «Пароль неверен», при отмене – «Вход отменён».
*/

var login = prompt('Введите логин','');
var pwd;

if (login === 'Админ') {
  pwd = prompt('Введите пароль','');
    
  if (pwd === 'Чёрный Властелин') {
    alert('Добро пожаловать!');
  } else if (pwd !== 'Чёрный Властелин') {
    alert('Пароль неверен');
  } else if (pwd == null) {
    alert('Вход отменен');
  }
} else if (login == null) {
  alert('Вход отменен');
} else {
  alert('Я вас не знаю');
}


/*
Перепишите if с использованием оператора '?':
*/
if (a + b < 4) {
  result = 'Мало';
} else {
  result = 'Много';
}


result = (a + b < 4) ? 'Мало' : 'Много';


/*
Перепишите if..else с использованием нескольких операторов '?'.
Для читаемости – оформляйте код в несколько строк.
*/
var message;

if (login == 'Вася') {
  message = 'Привет';
} else if (login == 'Директор') {
  message = 'Здравствуйте';
} else if (login == '') {
  message = 'Нет логина';
} else {
  message = '';
}

var message = (login == 'Вася') ? 'Привет' :
              (login == 'Директор') ? 'Здравствуйте' :
              (login == '') ? 'Нет логина' : 
              '';


/*
ЛОГИЧЕСКИЕ ОПЕРАТОРЫ

Что выведет код ниже?
*/
alert( null || 2 || undefined );		// 2
alert( alert(1) || 2 || alert(3) ); 	// 1, потом 2
alert( 1 && null && 2 );				// null
alert( alert(1) && alert(2) );			// 1, потом undefined
alert( null || 2 && 3 || 4 );			// 3


/*
Напишите условие if для проверки того факта, 
что переменная age находится между 14 и 90 включительно.
«Включительно» означает, что концы промежутка включены, 
то есть age может быть равна 14 или 90.
*/

if ( (age >= 14) && (age <= 90))


/*
Напишите условие if для проверки того факта, 
что age НЕ находится между 14 и 90 включительно.
Сделайте два варианта условия: первый с использованием оператора НЕ !, 
второй – без этого оператора.
*/

if ( !(age >= 14 && age <= 90))
if (age < 14 || age > 90)


/*
Какие из этих if верны, т.е. выполнятся?
Какие конкретно значения будут результатами выражений в условиях if(...)?
*/

if (-1 || 0) alert( 'первое' );				// выполнится
if (-1 && 0) alert( 'второе' );				// не выполнится
if (null || -1 && 1) alert( 'третье' );		// выполнится


/*
Подумайте, какой результат будет у выражений ниже. 
Тут не только преобразования типов. 
*/
"" + 1 + 0				// 10
"" - 1 + 0				// -1
true + false			// 1
6 / "3"					// 2
"2" * "3"				// 6
4 + 5 + "px"			// 9px
"$" + 4 + 5				// $45

"4" - 2					// 2

"4px" - 2				// NaN

7 / 0					// Infinity

"  -9\n" + 5			// -9\n5
"  -9\n" - 5			// -14
5 && 2					// 2

2 && 5					// 5

5 || 0					// 5

0 || 5					// 5
null + 1				// 1
undefined + 1			// NaN
null == "\n0\n"			// false
+null == +"\n0\n"		// true


/*
ЦИКЛЫ WHILE, FOR

Какое последнее значение выведет этот код? Почему?
*/
var i = 3;

while (i) {
  alert( i-- );			// 1
}


/*
Какие значения i выведет цикл while?
Для каждого цикла запишите, какие значения он выведет. Потом сравните с ответом.
*/
// Префиксный вариант
var i = 0;
while (++i < 5) alert( i );		// 1 2 3 4 

//Постфиксный вариант
var i = 0;
while (i++ < 5) alert( i );		// 1 2 3 4 5


/*
Какие значения i выведет цикл for?
Для каждого цикла запишите, какие значения он выведет. Потом сравните с ответом.
*/
//Постфиксная форма:
for (var i = 0; i < 5; i++) alert( i );		// 0 1 2 3 4

//Префиксная форма:
for (var i = 0; i < 5; ++i) alert( i );		// 0 1 2 3 4

/*
При помощи цикла for выведите чётные числа от 2 до 10.
*/

for (var i = 2; i <= 10; i +=2) alert( i );

//или
for (var i = 2; i <= 10; i++) {
  if (i % 2 == 0) {
    alert( i );
  }
}


/*
Перепишите код, заменив цикл for на while, без изменения поведения цикла.
 for (var i = 0; i < 3; i++) {
  alert( "номер " + i + "!" );
}
*/
var i = 0;
while(i < 3){
  alert( "номер " + i + "!" );
  i++;
}


/*
Повторять цикл, пока ввод неверен
Напишите цикл, который предлагает prompt ввести число, большее 100. 
Если посетитель ввёл другое число – попросить ввести ещё раз, и так далее.
Цикл должен спрашивать число пока либо посетитель не введёт число, большее 100, 
либо не нажмёт кнопку Cancel (ESC).
Предполагается, что посетитель вводит только числа. 
Предусматривать обработку нечисловых строк в этой задаче необязательно.
*/

var num = 0;
while (num <= 100 && !(num == null)){
  num = prompt('Введите число больше 100','')
}

//или

var num;
do {
  num = prompt("Введите число больше 100?", 0);
} while (num <= 100 && num != null);


/*
Вывести простые числа

Натуральное число, большее 1, называется простым, если оно ни на что не делится,
кроме себя и 1.
Другими словами, n>1 – простое, если при делении на любое число от 2 до n-1 
есть остаток.
Создайте код, который выводит все простые числа из интервала от 2 до 10. 
Результат должен быть: 2,3,5,7.
P.S. Код также должен легко модифицироваться для любых других интервалов.
*/

for (var i = 2; i <= 10; i++) {
  var temp=0;

  for (var j = 2; j < i; j++) {
    if (i % j == 0) temp++;
  }

  if (temp==0)  alert( i ); 
}

//или
nextPrime:
  for (var i = 2; i <= 10; i++) {

    for (var j = 2; j < i; j++) {
      if (i % j == 0) continue nextPrime;
    }

    alert( i ); // простое
  }


/*
КОНСТРУКЦИЯ SWITCH

Напишите "if", аналогичный "switch"
Напишите if..else, соответствующий следующему switch:
*/
switch (browser) {
  case 'IE':
    alert( 'О, да у вас IE!' );
    break;

  case 'Chrome':
  case 'Firefox':
  case 'Safari':
  case 'Opera':
    alert( 'Да, и эти браузеры мы поддерживаем' );
    break;

  default:
    alert( 'Мы надеемся, что и в вашем браузере все ок!' );
}


if (browser == 'IE'){
  alert( 'О, да у вас IE!' );
} else if (browser == 'Chrome' 
        || browser == 'Firefox' 
        || browser == 'Safari' 
        || browser == 'Opera'){
  ( 'Да, и эти браузеры мы поддерживаем' );
} else {
  alert( 'Мы надеемся, что и в вашем браузере все ок!' );
}


/*
Переписать if'ы в switch
Перепишите код с использованием одной конструкции switch:
*/
var a = +prompt('a?', '');

if (a == 0) {
  alert( 0 );
}
if (a == 1) {
  alert( 1 );
}

if (a == 2 || a == 3) {
  alert( '2,3' );
}


var a = +prompt('a?', '');
switch (a) {
  case 0:
    alert( 0 );
    break;
  case 1:
    alert( 1 );
    break;
  case 2:
  case 3:
    alert( '2,3' );
    break;
}


/*
ФУНКЦИИ

Перепишите функцию, используя оператор '?' или '||'
Следующая функция возвращает true, если параметр age больше 18. 
В ином случае она задаёт вопрос confirm и возвращает его результат.
*/
function checkAge(age) {
  if (age > 18) {
    return true;
  } else {
    return confirm('Родители разрешили?');
  }
}
/*
Перепишите функцию, чтобы она делала то же самое, но без if, в одну строку. 
Сделайте два варианта функции checkAge:
*/

//Используя оператор '?'
function checkAge(age) {
  return (age > 18) ? true : confirm('Родители разрешили?');
}

//Используя оператор ||
function checkAge(age) {
  return (age > 18) || confirm('Родители разрешили?');
}


/*
Функция min

Задача «Hello World» для функций :)
Напишите функцию min(a,b), которая возвращает меньшее из чисел a,b.
Пример вызовов:
*/
min(2, 5) == 2
min(3, -1) == -1
min(1, 1) == 1

function min(a, b) {
  return (a < b) ? a : b;
}

//или
function min(a, b) {
  if (a < b) {
    return a;
  } else {
    return b;
  }
}


/*
Функция pow(x,n)

Напишите функцию pow(x,n), которая возвращает x в степени n. Иначе говоря, 
умножает x на себя n раз и возвращает результат.
*/
pow(3, 2) = 3 * 3 = 9
pow(3, 3) = 3 * 3 * 3 = 27
pow(1, 100) = 1 * 1 * ...*1 = 1
/*
Создайте страницу, которая запрашивает x и n, 
а затем выводит результат pow(x,n).
P.S. В этой задаче функция обязана поддерживать только натуральные значения n, 
т.е. целые от 1 и выше.
*/

var x = +prompt('Введите x');
var n = +prompt('Введите n');

function pow(x,n) {
  var result = x;
  for (var i = 1; i < n ; i++){
    result *=x;
  }
  return result;
}

if (n <= 1) {
  alert('Степень ' + n + 'не поддерживается, введите целую степень, большую 1');
} else {
  alert( pow(x, n) );
}


/*
РЕКУРСИЯ, СТЕК

Вычислить сумму чисел до данного
Напишите функцию sumTo(n), которая для данного n вычисляет сумму чисел 
от 1 до n, например:

sumTo(1) = 1
sumTo(2) = 2 + 1 = 3
sumTo(3) = 3 + 2 + 1 = 6
sumTo(4) = 4 + 3 + 2 + 1 = 10
...
sumTo(100) = 100 + 99 + ... + 2 + 1 = 5050

Сделайте три варианта решения:
- С использованием цикла.
- Через рекурсию, т.к. sumTo(n) = n + sumTo(n-1) для n > 1.
- С использованием формулы для суммы арифметической прогрессии.

Пример работы вашей функции:
function sumTo(n) { ... ваш код ...  }
alert( sumTo(100) ); // 5050

Какой вариант решения самый быстрый? Самый медленный? Почему?
Можно ли при помощи рекурсии посчитать sumTo(100000)? Если нет, то почему?
*/

//- С использованием цикла.
function sumTo (n) {
  var result = 0;
  while (n > 0) {
    result +=n;
    n--;
  }

  return result;
}

alert( sumTo(100) );

//- Через рекурсию, т.к. sumTo(n) = n + sumTo(n-1) для n > 1.
function sumTo(n) {
  if (n == 1) return 1;
  return n + sumTo(n - 1);
}

alert( sumTo(100) );

//- С использованием формулы для суммы арифметической прогрессии.
function sumTo(n) {
  return n * (n + 1) / 2;
}

alert( sumTo(100) );


/*
Вычислить факториал

Факториа́л числа – это число, умноженное на «себя минус один», 
затем на «себя минус два» 
и так далее, до единицы. Обозначается n!
Определение факториала можно записать как:
n! = n * (n - 1) * (n - 2) * ...*1
Примеры значений для разных n:
1! = 1
2! = 2 * 1 = 2
3! = 3 * 2 * 1 = 6
4! = 4 * 3 * 2 * 1 = 24
5! = 5 * 4 * 3 * 2 * 1 = 120
Задача – написать функцию factorial(n), которая возвращает факториал числа n!, 
используя рекурсивный вызов.
alert( factorial(5) ); // 120
Подсказка: обратите внимание, что n! можно записать как n * (n-1)!. 
Например: 3! = 3*2! = 3*2*1! = 6
*/

function factorial(n) {
  if (n==1) return 1;
  return n * factorial(n-1)
}

alert(factorial(3));

//или
function factorial(n) {
  return (n != 1) ? n * factorial(n - 1) : 1;
}

alert( factorial(5) ); // 120
//или

function factorial(n) {
  return n ? n * factorial(n - 1) : 1;
}

alert( factorial(5) ); // 120


/*
Числа Фибоначчи

Последовательность чисел Фибоначчи имеет формулу Fn = Fn-1 + Fn-2. 
То есть, следующее число получается как сумма двух предыдущих.
Первые два числа равны 1, затем 2(1+1), затем 3(1+2), 5(2+3) 
и так далее: 1, 1, 2, 3, 5, 8, 13, 21....
Числа Фибоначчи тесно связаны с золотым сечением и множеством природных явлений 
вокруг нас.
Напишите функцию fib(n), которая возвращает n-е число Фибоначчи. 
Пример работы:
function fib(n) {  ваш код  }

alert( fib(3) ); // 2
alert( fib(7) ); // 13
alert( fib(77)); // 5527939700884757
Все запуски функций из примера выше должны срабатывать быстро.
*/

function fib(n) {
  return n <= 1 ? n : fib(n - 1) + fib(n - 2);
}

alert (fib(7));





/****************************************************************************
* СТРУКТУРЫ ДАННЫХ
****************************************************************************/


/* 
2. ЧИСЛА
*/

/*
Интерфейс суммы
Создайте страницу, которая предлагает ввести два числа и выводит их сумму.
P.S. Есть «подводный камень» при работе с типами.
*/

var x = +prompt('Введите x');
var y = +prompt('Введите y');

function sum(x, y) {
  return x + y;
}


/*
Почему 6.35.toFixed(1) == 6.3?
В математике принято, что 5 округляется вверх, например:

alert( 1.5.toFixed(0) ); // 2
alert( 1.35.toFixed(1) ); // 1.4

Но почему в примере ниже 6.35 округляется до 6.3?
alert( 6.35.toFixed(1) ); // 6.3
*/

alert( 6.35.toFixed(20) ); // 6.34999999999999964473
//Интерпретатор видит число как 6.34..., поэтому и округляет вниз.


/*
Сложение цен

Представьте себе электронный магазин. Цены даны с точностью до копейки(цента, 
евроцента и т.п.).
Вы пишете интерфейс для него. Основная работа происходит на сервере, 
но и на клиенте все должно быть хорошо. 
Сложение цен на купленные товары и умножение их на количество является обычной 
операцией.
Получится глупо, если при заказе двух товаров с ценами 0.10$ и 0.20$ 
человек получит общую стоимость 0.30000000000000004$:

alert( 0.1 + 0.2 + '$' );
Что можно сделать, чтобы избежать проблем с ошибками округления?
*/

//Для округления до нужного знака используйте +n.toFixed(p) 
//или трюк с умножением и делением на 10p.


/*
Бесконечный цикл по ошибке
Этот цикл – бесконечный. Почему?
*/
var i = 0;
while (i != 10) {
  i += 0.2;
}
//Потому что i никогда не станет равным 10.
//Запустите, чтобы увидеть реальные значения i:
 var i = 0;
while (i < 11) {
  i += 0.2;
  if (i > 9.8 && i < 10.2) alert( i );  //9.999999999999996
										//10.199999999999996
}


/*
Как получить дробную часть числа?
Напишите функцию getDecimal(num), которая возвращает десятичную часть числа:
*/
alert( getDecimal(12.345) ); 	// 0.345
alert( getDecimal(1.2) ); 		// 0.2
alert( getDecimal(-1.2) ); 		// 0.2
//P.S. Постарайтесь не использовать toFixed


function getDecimal (x) {
  var n = x - Math.round(x);
  return Math.round(n * 100) / 100;
}

//или более точно
function getDecimal(num) {
  var str = "" + num;
  var zeroPos = str.indexOf(".");
  if (zeroPos == -1) return 0;
  str = str.slice(zeroPos);
  return +str;
}


/*
Формула Бине

Последовательность чисел Фибоначчи имеет формулу Fn = Fn-1 + Fn-2. 
То есть, следующее число получается как сумма двух предыдущих.
Первые два числа равны 1, затем 2(1+1), затем 3(1+2), 
5(2+3) и так далее: 1, 1, 2, 3, 5, 8, 13, 21....
Код для их вычисления (из задачи Числа Фибоначчи):
*/
function fib(n) {
  var a = 1,
    b = 0,
    x;
  for (i = 0; i < n; i++) {
    x = a + b;
    a = b
    b = x;
  }
  return b;
}
/*
Существует формула Бине, согласно которой Fn равно ближайшему целому для ϕn/√5, 
где ϕ=(1+√5)/2 – золотое сечение.

Напишите функцию fibBinet(n), которая будет вычислять Fn, используя эту формулу. 
Проверьте её для значения F77 
(должно получиться fibBinet(77) = 5527939700884757).

Одинаковы ли результаты, полученные при помощи кода fib(n) выше и по формуле 
Бине? Если нет, то почему и какой из них верный?
*/
function fibBinet(num) {
  var f = (1 + Math.sqrt(5))/2;
  return Math.round(Math.pow(f,num) / Math.sqrt(5));
}

document.write(fibBinet(77));   // 5527939700884755
alert( fib(77) );				// 5527939700884757, не совпадает!

//Причина – в ошибках округления, ведь √5 – бесконечная дробь.
//Ошибки округления при вычислениях множатся и, в итоге, дают расхождение.


/*
Случайное из интервала (0, max)
Напишите код для генерации случайного значения в диапазоне от 0 до max, 
не включая max.
*/
function renderMax (max) {
  return Math.random() * max;
}


/*
Случайное из интервала (min, max)
Напишите код для генерации случайного числа от min до max, не включая max.
*/
function renderMinMax (min, max) {
  return min + Math.random() * (max - min);
}


/*
Случайное целое от min до max
Напишите функцию randomInteger(min, max) для генерации случайного целого числа 
между min и max, включая min,max как возможные значения.
Любое число из интервала min..max должно иметь одинаковую вероятность.
*/
function randomInteger(min, max) {
  return Math.round(min + Math.random() * (max - min));
}

function randomInteger(min, max) {
  var rand = min - 0.5 + Math.random() * (max - min + 1)
  rand = Math.round(rand);
  return rand;
}

function randomInteger(min, max) {
  var rand = min + Math.random() * (max + 1 - min);
  rand = Math.floor(rand);
  return rand;
}


/*
3. СТРОКИ
*/

/*
Сделать первый символ заглавным
Напишите функцию ucFirst(str), которая возвращает строку str с заглавным первым 
символом, например:
ucFirst("вася") == "Вася";
ucFirst("") == ""; // нет ошибок при пустой строке
P.S. В JavaScript нет встроенного метода для этого. Создайте функцию, используя 
toUpperCase() и charAt().
*/
function ucFirst(str) {
  if (str == "") {
    return "";
  } else {
    var newStr = str[0].toUpperCase();
    
    for (var i = 1; i < str.length; i++){
      newStr += str[i];
    }
    
    return newStr;
  }
}

document.write(ucFirst("вася"));
document.write(ucFirst(""));

//или
function ucFirst(str) {
  // только пустая строка в логическом контексте даст false
  if (!str) return str;

  return str[0].toUpperCase() + str.slice(1);
}


/*
Проверьте спам

Напишите функцию checkSpam(str), которая возвращает true, 
если строка str содержит „viagra“ или „XXX“, а иначе false.
Функция должна быть нечувствительна к регистру:
checkSpam('buy ViAgRA now') == true
checkSpam('free xxxxx') == true
checkSpam("innocent rabbit") == false
*/
function checkSpam(str) {
  var newStr = str.toLowerCase();

  if (newStr.indexOf("viagra") > -1 ||
    newStr.indexOf("xxx") > -1){

      return true;
  }

  return false;
}

// или
function checkSpam(str) {
  var lowerStr = str.toLowerCase();

  return !!(~lowerStr.indexOf('viagra') || ~lowerStr.indexOf('xxx'));
}


/*
Усечение строки

Создайте функцию truncate(str, maxlength), которая проверяет длину строки str, 
и если она превосходит maxlength – заменяет конец str на "...", так чтобы ее 
длина стала равна maxlength.
Результатом функции должна быть (при необходимости) усечённая строка.
Например:
truncate("Вот, что мне хотелось бы сказать на эту тему:", 20) = 
"Вот, что мне хоте..."

truncate("Всем привет!", 20) = "Всем привет!"
Эта функция имеет применение в жизни. Она используется, 
чтобы усекать слишком длинные темы сообщений.
P.S. В кодировке Unicode существует специальный символ «троеточие»: … 
(HTML: &hellip;), но в этой задаче подразумеваются именно три точки подряд.
*/
function truncate(str, maxlength) {
  if (str.length > maxlength) {
    return str.slice(0, maxlength - 3) + "..."
  }

  return str
}

// или 
function truncate(str, maxlength) {
  return (str.length > maxlength) ?
    str.slice(0, maxlength - 3) + '...' : str;
}


/*
Выделить число

Есть стоимость в виде строки: "$120". То есть, первым идёт знак валюты, 
а затем – число.
Создайте функцию extractCurrencyValue(str), которая будет из такой строки 
выделять число-значение, в данном случае 120.
*/
function extractCurrencyValue(str) {
  return +str.slice(1);
}


/*
4. ОБЪЕКТЫ КАК АССОЦИАТИВНЫЕ МАССИВЫ
*/

/*
Первый объект

Мини-задача на синтаксис объектов. Напишите код, по строке на каждое действие.
Создайте пустой объект user.
Добавьте свойство name со значением Вася.
Добавьте свойство surname со значением Петров.
Поменяйте значение name на Сергей.
Удалите свойство name из объекта.
*/
var user = {};
user.name = "Вася";
user.surname = "Петров";
user.name = "Сергей";
delete user.name;


/*
Определите, пуст ли объект

Создайте функцию isEmpty(obj), которая возвращает true, если в объекте нет 
свойств и false – если хоть одно свойство есть.
Работать должно так:
*/
function isEmpty(obj) {
   //ваш код 
}

var schedule = {};
alert( isEmpty(schedule) ); // true
schedule["8:30"] = "подъём";
alert( isEmpty(schedule) ); // false
*/

function isEmpty(obj) {
  for (var key in obj) {
    return false;
  }
  return true;
}


/*
Сумма свойств

Есть объект salaries с зарплатами. Напишите код, который выведет сумму всех 
зарплат.
Если объект пустой, то результат должен быть 0.
Например:
*/
"use strict";
var salaries = {
  "Вася": 100,
  "Петя": 300,
  "Даша": 250
};
//... ваш код выведет 650
//P.S. Сверху стоит use strict, чтобы не забыть объявить переменные.
var amountOfAllSalaries = 0;

function sumSalaries(obj) {
  for (var key in obj) {
    amountOfAllSalaries += obj[key];
  }

  return amountOfAllSalaries > 0 ? amountOfAllSalaries : 0;
}

document.writeln( sumSalaries(salaries) ); 


/*
Свойство с наибольшим значением

Есть объект salaries с зарплатами. Напишите код, который выведет имя сотрудника,
у которого самая большая зарплата.
Если объект пустой, то пусть он выводит «нет сотрудников».
Например:
"use strict";
var salaries = {
  "Вася": 100,
  "Петя": 300,
  "Даша": 250
};
// ... ваш код выведет "Петя"
*/
"use strict";
var salaries = {
  "Вася": 100,
  "Петя": 300,
  "Даша": 250
};

var maxSalaries = 0;
var name;

function sumSalaries(obj) {
  for (var key in obj) {
    if (obj[key] > maxSalaries) {
      name = key;
      maxSalaries = obj[key];
    }
  }

  return maxSalaries > 0 ? name : "нет сотрудников";	
}

document.writeln( sumSalaries(salaries) ); 

// или

var max = 0;
var maxName = "";
for (var name in salaries) {
  if (max < salaries[name]) {
    max = salaries[name];
    maxName = name;
  }
}

alert( maxName || "нет сотрудников" );


/*
Умножьте численные свойства на 2

Создайте функцию multiplyNumeric, которая получает объект и умножает все 
численные свойства на 2. Например:
// до вызова
var menu = {
  width: 200,
  height: 300,
  title: "My menu"
};
multiplyNumeric(menu);
// после вызова
menu = {
  width: 400,
  height: 600,
  title: "My menu"
};
P.S. Для проверки на число используйте функцию:
function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n)
}
*/

var menu = {
  width: 200,
  height: 300,
  title: "My menu"
};

function multiplyNumeric (obj) {
  for (var key in obj) {
    if (isNumeric(obj[key])) {
      obj[key] *= 2;
    }
  }
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n)
}

multiplyNumeric(menu); 

for (var key in menu) {
  document.writeln(menu[key]);  // 400 600  My menu
}

// or
var menu = {
  width: 200,
  height: 300,
  title: "My menu"
};

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function multiplyNumeric(obj) {
  for (var key in obj) {
    if (isNumeric(obj[key])) {
      obj[key] *= 2;
    }
  }
}

multiplyNumeric(menu);

alert("menu width=" + menu.width + 
      " height=" + menu.height + 
      " title=" + menu.title );


/*
7. Массивы с числовыми индексами

Получить последний элемент массива
Как получить последний элемент из произвольного массива?
У нас есть массив goods. Сколько в нем элементов – не знаем, но можем прочитать 
из goods.length.
Напишите код для получения последнего элемента goods.
*/
var lastItem = goods[goods.length - 1]; // получить последний элемент


/*
Добавить новый элемент в массив

Как добавить элемент в конец произвольного массива?
У нас есть массив goods. Напишите код для добавления в его конец значения 
«Компьютер».
*/

var goods = ["a", "b"];
goods.push("Компьютер");

document.writeln(goods);

//or
goods[goods.length] = 'Компьютер'


/*
Создание массива

Задача из 5 шагов-строк:
Создайте массив styles с элементами «Джаз», «Блюз».
Добавьте в конец значение «Рок-н-Ролл»
Замените предпоследнее значение с конца на «Классика». 
Код замены предпоследнего значения должен работать для массивов любой длины.
Удалите первое значение массива и выведите его alert.
Добавьте в начало значения «Рэп» и «Регги».

Массив в результате каждого шага:
Джаз, Блюз
Джаз, Блюз, Рок-н-Ролл
Джаз, Классика, Рок-н-Ролл
Классика, Рок-н-Ролл
Рэп, Регги, Классика, Рок-н-Ролл
*/

var styles = ["Джаз", "Блюз"];
styles.push("Рок-н-Ролл");
styles[styles.length - 2] = "Классика";
alert( styles.shift() );
styles.unshift("Рэп", "Регги");


/*
Получить случайное значение из массива

Напишите код для вывода alert случайного значения из массива:
var arr = ["Яблоко", "Апельсин", "Груша", "Лимон"];
P.S. Код для генерации случайного целого от min to max включительно:
var rand = min + Math.floor(Math.random() * (max + 1 - min));
*/

var arr = ["Яблоко", "Апельсин", "Груша", "Лимон"];
//var rand = min + Math.floor(Math.random() * (max + 1 - min));
var rand = Math.floor(Math.random() * (arr.length));

alert (arr[rand]);


/*
Создайте калькулятор для введённых значений

Напишите код, который:
Запрашивает по очереди значения при помощи prompt и сохраняет их в массиве.
Заканчивает ввод, как только посетитель введёт пустую строку, не число или 
нажмёт «Отмена».
При этом ноль 0 не должен заканчивать ввод, это разрешённое число.
Выводит сумму всех значений массива
*/

function sumArr() {
  var arr = [];
  var sum = 0;
  var num = 0;

  do{
    num = prompt('Введите число');

    if (isNumeric(num)) {
      arr.push(+num);
    } else {
      for (var key in arr) {
        sum += arr[key];
      }
    }
  } while (isNumeric(num) && num != null);

  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  return sum;
}

document.write(sumArr());

// or
var numbers = [];

while (true) {
  var value = prompt("Введите число", 0);
  if (value === "" || value === null || isNaN(value)) break;
  numbers.push(+value);
}

var sum = 0;
for (var i = 0; i < numbers.length; i++) {
  sum += numbers[i];
}

alert( sum );


/*
Чему равен элемент массива?
Что выведет этот код?
*/
var arr = [1, 2, 3];

var arr2 = arr;
arr2[0] = 5;

alert( arr[0] ); 		// 5
alert( arr2[0] );		// 5


/*
Поиск в массиве

Создайте функцию find(arr, value), которая ищет в массиве arr значение value и 
возвращает его номер, если найдено, или -1, если не найдено.
Например:
arr = ["test", 2, 1.5, false];
find(arr, "test"); 				// 0
find(arr, 2); 					// 1
find(arr, 1.5); 				// 2
find(arr, 0); 					// -1
*/

function find(arr, value) {
  for (var key in arr) {
    if (arr[key] === value) {
      return key;
    }
  }
  
  return -1;
}

// or
function find(array, value) {

  for (var i = 0; i < array.length; i++) {
    if (array[i] == value) return i;
  }

  return -1;
}

//or
function find(array, value) {
  if (array.indexOf) { // если метод существует
    return array.indexOf(value);
  }

  for (var i = 0; i < array.length; i++) {
    if (array[i] === value) return i;
  }

  return -1;
}

var arr = ["a", -1, 2, "b"];

var index = find(arr, 2);

alert( index );

//or

// создаем пустой массив и проверяем поддерживается ли indexOf
if ([].indexOf) {

  var find = function(array, value) {
    return array.indexOf(value);
  }

} else {
  var find = function(array, value) {
    for (var i = 0; i < array.length; i++) {
      if (array[i] === value) return i;
    }

    return -1;
  }

}
//Этот способ – лучше всего, т.к. не требует при каждом запуске find проверять 
//поддержку indexOf.

/*
Фильтр диапазона

Создайте функцию filterRange(arr, a, b), которая принимает массив чисел arr и 
возвращает новый массив, который содержит только числа из arr из диапазона 
от a до b. То есть, проверка имеет вид a ≤ arr[i] ≤ b. 
Функция не должна менять arr.

Пример работы:
var arr = [5, 4, 3, 8, 0];
var filtered = filterRange(arr, 3, 5);
// теперь filtered = [5, 4, 3]
// arr не изменился
*/

function filterRange(arr, a, b) {
  var result = [];

  for (var i = 0; i < arr.length; i++) {
    if (arr[i] >= a && arr[i] <= b) {
      result.push(arr[i]);
    }
  }

  return result;
}

var arr = [5, 4, 3, 8, 0];

var filtered = filterRange(arr, 3, 5);
alert( filtered );


/*
Решето Эратосфена

Целое число, большее 1, называется простым, если оно не делится нацело ни на 
какое другое, кроме себя и 1.
Древний алгоритм «Решето Эратосфена» для поиска всех простых чисел до n 
выглядит так:
Создать список последовательных чисел от 2 до n: 2, 3, 4, ..., n.
Пусть p=2, это первое простое число.
Зачеркнуть все последующие числа в списке с разницей в p, т.е. 2*p, 3*p, 
4*p и т.д. В случае p=2 это будут 4,6,8....
Поменять значение p на первое не зачеркнутое число после p.
Повторить шаги 3-4 пока p2 < n.
Все оставшиеся не зачеркнутыми числа – простые.
Посмотрите также анимацию алгоритма.
Реализуйте «Решето Эратосфена» в JavaScript, используя массив.
Найдите все простые числа до 100 и выведите их сумму.
*/

// шаг 1
var arr = [];

for (var i = 2; i < 100; i++) {
  arr[i] = true
}

// шаг 2
var p = 2;

do {
  // шаг 3
  for (i = 2 * p; i < 100; i += p) {
    arr[i] = false;
  }

  // шаг 4
  for (i = p + 1; i < 100; i++) {
    if (arr[i]) break;
  }

  p = i;
} while (p * p < 100); // шаг 5

// шаг 6 (готово)
// посчитать сумму
var sum = 0;
for (i = 0; i < arr.length; i++) {
  if (arr[i]) {
    sum += i;
  }
}

alert( sum );


/*
Подмассив наибольшей суммы

На входе массив чисел, например: arr = [1, -2, 3, 4, -9, 6].
Задача – найти непрерывный подмассив arr, сумма элементов которого максимальна.
Ваша функция должна возвращать только эту сумму.

Например:
getMaxSubSum([-1, 2, 3, -9]) = 5 (сумма выделенных)
getMaxSubSum([2, -1, 2, 3, -9]) = 6
getMaxSubSum([-1, 2, 3, -9, 11]) = 11
getMaxSubSum([-2, -1, 1, 2]) = 3
getMaxSubSum([100, -9, 2, -3, 5]) = 100
getMaxSubSum([1, 2, 3]) = 6 (неотрицательные - берем всех)
Если все элементы отрицательные, то не берём ни одного элемента и считаем сумму 
равной нулю:
getMaxSubSum([-1, -2, -3]) = 0
Постарайтесь придумать решение, которое работает за O(n2), а лучше за O(n) 
операций.
*/

function getMaxSubSum(arr) {
  var maxSum = 0; // если совсем не брать элементов, то сумма 0

  for (var i = 0; i < arr.length; i++) {
    var sumFixedStart = 0;
    for (var j = i; j < arr.length; j++) {
      sumFixedStart += arr[j];
      maxSum = Math.max(maxSum, sumFixedStart);
    }
  }

  return maxSum;
}

alert( getMaxSubSum([-1, 2, 3, -9]) ); // 5
alert( getMaxSubSum([-1, 2, 3, -9, 11]) ); // 11
alert( getMaxSubSum([-2, -1, 1, 2]) ); // 3
alert( getMaxSubSum([1, 2, 3]) ); // 6
alert( getMaxSubSum([100, -9, 2, -3, 5]) ); // 100

//or
function getMaxSubSum(arr) {
  var maxSum = 0,
    partialSum = 0;
  for (var i = 0; i < arr.length; i++) {
    partialSum += arr[i];
    maxSum = Math.max(maxSum, partialSum);
    if (partialSum < 0) partialSum = 0;
  }
  return maxSum;
}


/*
8. Массивы: методы

Добавить класс в строку
В объекте есть свойство className, которое содержит список «классов» – слов, 
разделенных пробелом:
var obj = {
  className: 'open menu'
}
Создайте функцию addClass(obj, cls), которая добавляет в список класс cls, 
но только если его там еще нет:
addClass(obj, 'new'); // obj.className='open menu new'
addClass(obj, 'open'); // без изменений (класс уже существует)
addClass(obj, 'me'); // obj.className='open menu new me'

alert( obj.className ); // "open menu new me"
P.S. Ваша функция не должна добавлять лишних пробелов.
*/

function addClass(obj, cls) {
  var classes = obj.className ? obj.className.split(' ') : [];

  for (var i = 0; i < classes.length; i++) {
    if (classes[i] == cls) return; // класс уже есть
  }

  classes.push(cls); // добавить
  obj.className = classes.join(' '); // и обновить свойство
}

var obj = {
  className: 'open menu'
};

addClass(obj, 'new');
addClass(obj, 'open');
addClass(obj, 'me');
alert(obj.className) // open menu new me


/*
Перевести текст вида border-left-width в borderLeftWidth

Напишите функцию camelize(str), которая преобразует строки вида 
«my-short-string» в «myShortString».
То есть, дефисы удаляются, а все слова после них получают заглавную букву.
Например:
camelize("background-color") == 'backgroundColor';
camelize("list-style-image") == 'listStyleImage';
camelize("-webkit-transition") == 'WebkitTransition';
Такая функция полезна при работе с CSS.
P.S. Вам пригодятся методы строк charAt, split и toUpperCase.
*/

function camelize(str) {
  var arr = str.split('-');
  var newStr = "";

  for (var i = 0; i < arr.length; i++){
    newStr += ' ' + ucFirst(arr[i])
  }

  function ucFirst(str) {
    if (str == "") {
      return "";
    } else {
      var newStr = str[0].toUpperCase();
      for (var i = 1; i < str.length; i++){
        newStr += str[i];
      }
      return newStr;
    }
  }

  return newStr;
}

// or
function camelize(str) {
  var arr = str.split('-');

  for (var i = 1; i < arr.length; i++) {
    // преобразовать: первый символ с большой буквы
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
  }

  return arr.join('');
}


/*
Функция removeClass

У объекта есть свойство className, которое хранит список «классов» – слов, 
разделенных пробелами:

var obj = {
  className: 'open menu'
};
Напишите функцию removeClass(obj, cls), которая удаляет класс cls, если он есть:

removeClass(obj, 'open'); // obj.className='menu'
removeClass(obj, 'blabla'); // без изменений (нет такого класса)
P.S. Дополнительное усложнение. Функция должна корректно обрабатывать 
дублирование класса в строке:

obj = {
  className: 'my menu menu'
};
removeClass(obj, 'menu');
alert( obj.className ); // 'my'
Лишних пробелов после функции образовываться не должно.
*/

function removeClass(obj, cls) {
  var classes = obj.className.split(' ');

  for (var i = 0; i < classes.length; i++) {
    if (classes[i] == cls) {
      classes.splice(i, 1); // удалить класс
      i--; // (*)
    }
  }
  obj.className = classes.join(' ');

}

var obj = {
  className: 'open menu menu'
}

removeClass(obj, 'blabla');
removeClass(obj, 'menu')
alert(obj.className) // open


/*
Фильтрация массива "на месте"

Создайте функцию filterRangeInPlace(arr, a, b), которая получает массив 
с числами arr и удаляет из него все числа вне диапазона a..b. То есть, 
проверка имеет вид a ≤ arr[i] ≤ b. Функция должна менять сам массив 
и ничего не возвращать.

Например:
arr = [5, 3, 8, 1];
filterRangeInPlace(arr, 1, 4); // удалены числа вне диапазона 1..4
alert( arr ); // массив изменился: остались [3, 1]
*/

function filterRangeInPlace(arr, a, b) {
  for (var i = 0; i < arr.length; i++) {
    if (!(arr[i] >= a && arr[i] <= b)) {
      arr.splice(i, 1);
    }
  }
}

var arr = [5, 3, 8, 1];
filterRangeInPlace(arr, 1, 4); 
document.write( arr);

// or
function filterRangeInPlace(arr, a, b) {
  for (var i = 0; i < arr.length; i++) {
    var val = arr[i];
    if (val < a || val > b) {
      arr.splice(i--, 1);
    }
  }
}


/*
Сортировать в обратном порядке

Как отсортировать массив чисел в обратном порядке?
var arr = [5, 2, 1, -10, 8];
// отсортируйте?
alert( arr ); // 8, 5, 2, 1, -10
*/

var arr = [5, 2, 1, -10, 8];
arr.sort(function(a, b) { return a-b })
alert( arr );  // -10,1,2,5,8

// or
function compareReversed(a, b) {
  return b - a;
}

arr.sort(compareReversed);

alert( arr );


/*
Скопировать и отсортировать массив

Есть массив строк arr. Создайте массив arrSorted – из тех же элементов, 
но отсортированный.
Исходный массив не должен меняться.
var arr = ["HTML", "JavaScript", "CSS"];
// ... ваш код ...
alert( arrSorted ); // CSS, HTML, JavaScript
alert( arr ); // HTML, JavaScript, CSS (без изменений)
Постарайтесь сделать код как можно короче.
*/

var arr = ["HTML", "JavaScript", "CSS"];
var arrSorted = arr.slice().sort();
alert( arrSorted ); // CSS, HTML, JavaScript
alert( arr ); // HTML, JavaScript, CSS (без изменений)


/*
Случайный порядок в массиве
важность: 3
Используйте функцию sort для того, чтобы «перетрясти» элементы массива в 
случайном порядке.
var arr = [1, 2, 3, 4, 5];
arr.sort(ваша функция);
alert( arr ); // элементы в случайном порядке, например [3,5,1,2,4]
*/

var arr = [1, 2, 3, 4, 5];

function compareRandom(a, b) {
  return Math.random() - 0.5;
}
arr.sort(compareRandom);
alert( arr ); 


/*
Сортировка объектов

Напишите код, который отсортирует массив объектов people по полю age.
Например:
var vasya = { name: "Вася", age: 23 };
var masha = { name: "Маша", age: 18 };
var vovochka = { name: "Вовочка", age: 6 };
var people = [ vasya , masha , vovochka ];
... ваш код ...
// теперь people: [vovochka, masha, vasya]
alert(people[0].age) // 6
Выведите список имён в массиве после сортировки.
*/

people.sort(function(a, b) { 
  return a.age-b.age; 
  })

// or
// Наша функция сравнения
function compareAge(personA, personB) {
  return personA.age - personB.age;
}

// проверка
var vasya = { name: "Вася", age: 23 };
var masha = { name: "Маша", age: 18 };
var vovochka = { name: "Вовочка", age: 6 };

var people = [ vasya , masha , vovochka ];

people.sort(compareAge);

// вывести
for(var i = 0; i < people.length; i++) {
  alert(people[i].name); // Вовочка Маша Вася
}


/*
Вывести односвязный список

Односвязный список – это структура данных, которая состоит из элементов, каждый 
из которых хранит ссылку на следующий. Последний элемент может не иметь ссылки, 
либо она равна null.

Например, объект ниже задаёт односвязный список, в next хранится ссылка на 
следующий элемент:

var list = {
  value: 1,
  next: {
    value: 2,
    next: {
      value: 3,
      next: {
        value: 4,
        next: null
      }
    }
  }
};

Альтернативный способ создания:
var list = { value: 1 };
list.next = { value: 2 };
list.next.next = { value: 3 };
list.next.next.next = { value: 4 };
Такая структура данных интересна тем, что можно очень быстро разбить список на 
части, объединить списки, удалить или добавить элемент в любое место, 
включая начало. При использовании массива такие действия требуют обширных 
перенумерований.

Задачи:

Напишите функцию printList(list), которая выводит элементы списка по очереди, 
при помощи цикла.
Напишите функцию printList(list) при помощи рекурсии.
Напишите функцию printReverseList(list), которая выводит элементы списка в 
обратном порядке, при помощи рекурсии. 
Для списка выше она должна выводить 4,3,2,1
Сделайте вариант printReverseList(list), использующий не рекурсию, а цикл.
Как лучше – с рекурсией или без?
*/
var list = { value: 1 };
list.next = { value: 2 };
list.next.next = { value: 3 };
list.next.next.next = { value: 4 };

// Вывод списка в цикле
function printList(list) {
  var tmp = list;

  while (tmp) {
    alert( tmp.value );
    tmp = tmp.next;
  }

}

// Вывод списка с рекурсией
function printList(list) {

  alert( list.value ); // (1)

  if (list.next) {
    printList(list.next); // (2)
  }

}

// Обратный вывод с рекурсией
function printReverseList(list) {

  if (list.next) {
    printReverseList(list.next);
  }

  alert( list.value );
}

// Обратный вывод без рекурсии
function printReverseList(list) {
  var arr = [];
  var tmp = list;

  while (tmp) {
    arr.push(tmp.value);
    tmp = tmp.next;
  }

  for (var i = arr.length - 1; i >= 0; i--) {
    alert( arr[i] );
  }
}


/*
Отфильтровать анаграммы

Анаграммы – слова, состоящие из одинакового количества одинаковых букв, 
но в разном порядке. Например:
воз - зов
киборг - гробик
корсет - костер - сектор
Напишите функцию aclean(arr), которая возвращает массив слов, 
очищенный от анаграмм.

Например:
var arr = ["воз", "киборг", "корсет", "ЗОВ", "гробик", "костер", "сектор"];
alert( aclean(arr) ); // "воз,киборг,корсет" или "ЗОВ,гробик,сектор"
Из каждой группы анаграмм должно остаться только одно слово, не важно какое.
*/

function aclean(arr) {
  // этот объект будем использовать для уникальности
  var obj = {};

  for (var i = 0; i < arr.length; i++) {
    // разбить строку на буквы, отсортировать и слить обратно
    var sorted = arr[i].toLowerCase().split('').sort().join(''); // (*)
    
    /* var sorted = arr[i] // ЗОВ
            .toLowerCase() // зов
            .split('') // ['з','о','в']
            .sort() // ['в','з','о']
            .join(''); // взо
    */
    
    obj[sorted] = arr[i]; // сохраняет только одно значение с таким ключом
  }

  var result = [];

  // теперь в obj находится для каждого ключа ровно одно значение
  for (var key in obj) result.push(obj[key]);

  return result;
}

var arr = ["воз", "киборг", "корсет", "ЗОВ", "гробик", "костер", "сектор"];

alert( aclean(arr) );


/*
Оставить уникальные элементы массива

Пусть arr – массив строк.
Напишите функцию unique(arr), которая возвращает массив, содержащий только 
уникальные элементы arr.

Например:
function unique(arr) {
   * ваш код *
}
var strings = ["кришна", "кришна", "харе", "харе",
  "харе", "харе", "кришна", "кришна", "8-()"
];
alert( unique(strings) ); // кришна, харе, 8-()
*/

// Решение перебором (медленное)
function unique(arr) {
  var result = [];

  nextInput:
    for (var i = 0; i < arr.length; i++) {
      var str = arr[i]; // для каждого элемента
      for (var j = 0; j < result.length; j++) { // ищем, был ли он уже?
        if (result[j] == str) continue nextInput; // если да, то следующий
      }
      result.push(str);
    }

  return result;
}

// Решение с объектом (быстрое)
function unique(arr) {
  var obj = {};

  for (var i = 0; i < arr.length; i++) {
    var str = arr[i];
    obj[str] = true; // запомнить строку в виде свойства объекта
  }

  return Object.keys(obj); // или собрать ключи перебором для IE8-
}

var strings = ["кришна", "кришна", "харе", "харе",
  "харе", "харе", "кришна", "кришна", "8-()"
];
alert( unique(strings) ); // кришна, харе, 8-()


/*
Массив: перебирающие методы

Перепишите цикл через map
Код ниже получает из массива строк новый массив, содержащий их длины:
var arr = ["Есть", "жизнь", "на", "Марсе"];
var arrLength = [];
for (var i = 0; i < arr.length; i++) {
  arrLength[i] = arr[i].length;
}
alert( arrLength ); // 4,5,2,5
Перепишите выделенный участок: уберите цикл, используйте вместо него метод map.
*/

var arr = ["Есть", "жизнь", "на", "Марсе"];

var arrLength = arr.map (function(name) {
  return name.length;
});
alert( arrLength ); // 4,5,2,5


/*
Массив частичных сумм

На входе массив чисел, например: arr = [1,2,3,4,5].
Напишите функцию getSums(arr), которая возвращает массив его частичных сумм.
Иначе говоря, вызов getSums(arr) должен возвращать новый массив из такого же 
числа элементов, в котором на каждой позиции должна быть сумма элементов arr 
до этой позиции включительно.
То есть:
для arr = [ 1, 2, 3, 4, 5 ]
getSums( arr ) = [ 1, 1+2, 1+2+3, 1+2+3+4, 1+2+3+4+5 ] = [ 1, 3, 6, 10, 15 ]
Еще пример: getSums([-2,-1,0,1]) = [-2,-3,-3,-2].
Функция не должна модифицировать входной массив.
В решении используйте метод arr.reduce.
*/

function getSums (arr) {
  return arr.map(function (item, i, arr) {
    return arr.slice(0, i + 1).reduce(function (sum, current) {
      return sum+current;
    })
  })
}

// or

function getSums(arr) {
  var result = [];
  if (!arr.length) return result;

  var totalSum = arr.reduce(function(sum, item) {
    result.push(sum);
    return sum + item;
  });
  result.push(totalSum);

  return result;
}


/*
Псевдомассив аргументов "arguments"

Проверка на аргумент-undefined
Как в функции отличить отсутствующий аргумент от undefined?
function f(x) {
  // ..ваш код..
  // выведите 1, если первый аргумент есть, и 0 - если нет
}
f(undefined); // 1
f(); // 0
*/

function f(x) {
    return arguments[0] === undefined ? 1 : 0;
}

// or
function f(x) {
  alert( arguments.length ? 1 : 0 );
}


/*
Сумма аргументов

Напишите функцию sum(...), которая возвращает сумму всех своих аргументов:
sum() = 0
sum(1) = 1
sum(1, 2) = 3
sum(1, 2, 3) = 6
sum(1, 2, 3, 4) = 10
*/

function sum() {
  var result = 0;

  for (var i = 0; i < arguments.length; i++) {
    result += arguments[i];
  }

  return result;
}


/*
Дата и Время

Создайте дату

Создайте объект Date для даты: 20 февраля 2012 года, 3 часа 12 минут.
Временная зона – местная. Выведите его на экран.
*/

var d = new Date(2012, 1, 20, 3, 12)
alert(d);  // Mon Feb 20 2012 03:12:00 GMT+0300 (Russia TZ 2 Standard Time)


/*
Имя дня недели

Создайте функцию getWeekDay(date), которая выводит текущий день недели в 
коротком формате „пн“, „вт“, … „вс“.
Например:
var date = new Date(2012,0,3);  // 3 января 2012
alert( getWeekDay(date) );      // Должно вывести 'вт'
*/

function getWeekDay(date) {
  var day = date.getDay();
  switch (day) {
    case(0):
      return "вс";
      break;
    case(1):
      return "пн";
      break;
    case(2):
      return "вт";
      break;
    case(3):
      return "чт";
      break;
    case(4):
      return "пт";
      break;
    case(5):
      return "сб";
      break;
    case(6):
      return "вс";
      break;
    default:
      return undefined;
   }   
}

// or
function getWeekDay(date) {
  var days = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'];

  return days[date.getDay()];
}


/*
День недели в европейской нумерации

Напишите функцию, getLocalDay(date) которая возвращает день недели для даты 
date.
День нужно возвратить в европейской нумерации, т.е. понедельник имеет номер 1, 
вторник номер 2, …, воскресенье – номер 7.
var date = new Date(2012, 0, 3);  // 3 янв 2012
alert( getLocalDay(date) );       // вторник, выведет 2
*/

function getLocalDay(date) {

  var day = date.getDay();

  if (day == 0) { // день 0 становится 7
    day = 7;
  }

  return day;
}


/*
День указанное количество дней назад

Создайте функцию getDateAgo(date, days), которая возвращает число, которое было 
days дней назад от даты date.
Например, для 2 января 2015:
var date = new Date(2015, 0, 2);
alert( getDateAgo(date, 1) ); // 1, (1 января 2015)
alert( getDateAgo(date, 2) ); // 31, (31 декабря 2014)
alert( getDateAgo(date, 365) ); // 2, (2 января 2014)
P.S. Важная деталь: в процессе вычислений функция не должна менять переданный 
ей объект date.
*/

function getDateAgo(date, days) {
  var dateCopy = new Date(date);

  dateCopy.setDate(date.getDate() - days);
  return dateCopy.getDate();
}


/*
Последний день месяца?

Напишите функцию getLastDayOfMonth(year, month), которая возвращает последний 
день месяца.
Параметры:
year – 4-значный год, например 2012.
month – месяц от 0 до 11.
Например, getLastDayOfMonth(2012, 1) = 29 (високосный год, февраль).
*/

function getLastDayOfMonth(year, month) {
  var date = new Date(year, month + 1, 0);
  return date.getDate();
}


/*
Сколько секунд уже прошло сегодня?

Напишите функцию getSecondsToday() которая возвращает, сколько секунд прошло с 
начала сегодняшнего дня.
Например, если сейчас 10:00 и не было перехода на зимнее/летнее время, то:
getSecondsToday() == 36000 // (3600 * 10)
Функция должна работать в любой день, т.е. в ней не должно быть конкретного 
значения сегодняшней даты.
*/

function getSecondsToday() {
  var now = new Date();

  // создать объект из текущей даты, без часов-минут-секунд
  var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

  var diff = now - today; // разница в миллисекундах
  return Math.floor(diff / 1000); // перевести в секунды
}

alert( getSecondsToday() );

// or
function getSecondsToday() {
  var d = new Date();
  return d.getHours() * 3600 + d.getMinutes() * 60 + d.getSeconds();
};


/*
Сколько секунд - до завтра?

Напишите функцию getSecondsToTomorrow() которая возвращает, сколько секунд 
осталось до завтра.
Например, если сейчас 23:00, то:
getSecondsToTomorrow() == 3600
P.S. Функция должна работать в любой день, т.е. в ней не должно быть конкретного 
значения сегодняшней даты.
*/

function getSecondsToTomorrow() {
  var now = new Date();

  // создать объект из завтрашней даты, без часов-минут-секунд
  var tomorrow = new Date(now.getFullYear(), now.getMonth(), 
  now.getDate()+1);

  var diff = tomorrow - now; // разница в миллисекундах
  return Math.round(diff / 1000); // перевести в секунды
}


/*
Вывести дату в формате дд.мм.гг

Напишите функцию formatDate(date), которая выводит дату date в формате дд.мм.гг:
Например:
var d = new Date(2014, 0, 30); // 30 января 2014
alert( formatDate(d) ); // '30.01.14'
P.S. Обратите внимание, ведущие нули должны присутствовать, то есть 
1 января 2001 должно быть 01.01.01, а не 1.1.1.
*/

function formatDate(date) {

  var dd = date.getDate();
  if (dd < 10) dd = '0' + dd;

  var mm = date.getMonth() + 1;
  if (mm < 10) mm = '0' + mm;

  var yy = date.getFullYear() % 100;
  if (yy < 10) yy = '0' + yy;

  return dd + '.' + mm + '.' + yy;
}


/*
Относительное форматирование даты

Напишите функцию formatDate(date), которая форматирует дату date так:
Если со времени date прошло менее секунды, то возвращает "только что".
Иначе если со времени date прошло менее минуты, то "n сек. назад".
Иначе если прошло меньше часа, то "m мин. назад".
Иначе полная дата в формате "дд.мм.гг чч:мм".
Например:
*/
function formatDate(date) { ваш код  }
alert( formatDate(new Date(new Date - 1)) ); // "только что"
alert( formatDate(new Date(new Date - 30 * 1000)) ); // "30 сек. назад"
alert( formatDate(new Date(new Date - 5 * 60 * 1000)) ); // "5 мин. назад"
alert( formatDate(new Date(new Date - 86400 * 1000)) ); // вчерашняя дата в 
//формате "дд.мм.гг чч:мм"

function formatDate(date) {
  var diff = new Date() - date; // разница в миллисекундах

  if (diff < 1000) { // прошло менее 1 секунды
    return 'только что';
  }

  var sec = Math.floor(diff / 1000); // округлить diff до секунд

  if (sec < 60) {
    return sec + ' сек. назад';
  }

  var min = Math.floor(diff / 60000); // округлить diff до минут
  if (min < 60) {
    return min + ' мин. назад';
  }

  // форматировать дату, с учетом того, что месяцы начинаются с 0
  var d = date;
  d = [
    '0' + d.getDate(),
    '0' + (d.getMonth() + 1),
    '' + d.getFullYear(),
    '0' + d.getHours(),
    '0' + d.getMinutes()
  ];

  for (var i = 0; i < d.length; i++) {
    d[i] = d[i].slice(-2);
  }

  return d.slice(0, 3).join('.') + ' ' + d.slice(3).join(':');
}





/****************************************************************************
* ЗАМЫКАНИЯ, ОБЛАСТЬ ВИДИМОСТИ
****************************************************************************/

/*
1. Глобальный объект

Window и переменная
Каков будет результат кода?
*/
if ("a" in window) {
  var a = 1;
}
alert( a );  // 1


/*
Window и переменная 2
Каков будет результат (перед a нет var)?
*/
if ("a" in window) {
  a = 1;
}
alert( a );  // error


/*
Window и переменная 3
Каков будет результат (перед a нет var, а ниже есть)?
*/
if ("a" in window) {
  a = 1;
}
var a;

alert( a );  // 1


/*
2. Замыкания, функции изнутри

Что выведет say в начале кода?

Что будет, если вызов say('Вася'); стоит в самом-самом начале, в первой строке 
кода?
*/
say('Вася'); // Что выведет? Не будет ли ошибки?

// Вася, undefined

var phrase = 'Привет';

function say(name) {
  alert( name + ", " + phrase );
}

/*
В какую переменную будет присвоено значение?

Каков будет результат выполнения этого кода?
*/
var value = 0;

function f() {
  if (1) {
    value = true;
  } else {
    var value = false;
  }

  alert( value );
}

f();
// 1. выведится true, т.к. внутрен value = true

//Изменится ли внешняя переменная value ?
// нет

//P.S. Какими будут ответы, если из строки var value = false убрать var?
// true - это изменится внешняя value


/*
var window

Каков будет результат выполнения этого кода? Почему?
*/

function test() {
  alert( window );      // undefined
  var window = 5;
  alert( window );      // 5
}
test();


/*
Вызов "на месте"

Каков будет результат выполнения кода? Почему?
P.S. Подумайте хорошо! Здесь все ошибаются! P.P.S. Внимание, здесь 
подводный камень! Ок, вы предупреждены.
*/
var a = 5       // error нет ; в конце !!!

(function() {
  alert(a)
})()


/*
Перекрытие переменной

Если во внутренней функции есть своя переменная с именем currentCount – можно 
ли в ней получить currentCount из внешней функции?
*/

function makeCounter() {
  var currentCount = 1;

  return function() {
    var currentCount;
    // можно ли здесь вывести currentCount из внешней функции (равный 1)?
    // Нет, нельзя.
    // Локальная переменная полностью перекрывает внешнюю.
  };
}

/*
Глобальный счётчик

Что выведут эти вызовы, если переменная currentCount находится вне makeCounter?
*/
var currentCount = 1;

function makeCounter() {
  return function() {
    return currentCount++;
  };
}

var counter = makeCounter();
var counter2 = makeCounter();

alert( counter() ); // ? 1
alert( counter() ); // ? 2

alert( counter2() ); // ? 3
alert( counter2() ); // ? 4


/*
3. [[Scope]] для new Function

4. Локальные переменные для объекта

Сумма через замыкание
Напишите функцию sum, которая работает так: sum(a)(b) = a+b.
Да, именно так, через двойные скобки (это не опечатка). Например:
sum(1)(2) = 3
sum(5)(-1) = 4
*/

function sum(a) {
  var currentSum = a;

  function f(b) {
    if (b !== undefined) {
      currentSum += b;
    } 
    return f;
  }

  f.toString = function() {
    return currentSum;
  };

  return f;
}

// or
function sum(a) {

  return function(b) {
    return a + b; // возьмет a из внешнего LexicalEnvironment
  };

}


/*
Функция - строковый буфер

В некоторых языках программирования существует объект «строковый буфер», 
который аккумулирует внутри себя значения. Его функционал состоит из двух 
возможностей:
Добавить значение в буфер.
Получить текущее содержимое.

Задача – реализовать строковый буфер на функциях в JavaScript, со следующим 
синтаксисом:
- Создание объекта: var buffer = makeBuffer();.
- Вызов makeBuffer должен возвращать такую функцию buffer, которая 
  при вызове buffer(value) добавляет значение в некоторое внутреннее хранилище, 
  а при вызове без аргументов buffer() – возвращает его.
  
Решение не должно использовать глобальные переменные.
Вот пример работы:*/

function makeBuffer() {  
  var text = '';

  return function(piece) {
    if (arguments.length == 0) { // вызов без аргументов
      return text;
    }
    text += piece;
  };
}

var buffer = makeBuffer();
// добавить значения к буферу
buffer('Замыкания');
buffer(' Использовать');
buffer(' Нужно!');

// получить текущее значение
alert( buffer() ); // Замыкания Использовать Нужно!
// Буфер должен преобразовывать все данные к строковому типу:

var buffer = makeBuffer();
buffer(0);
buffer(1);
buffer(0);

alert( buffer() ); // '010'


/*
Строковый буфер с очисткой

Добавьте буферу из решения задачи Функция - строковый буфер метод 
buffer.clear(), который будет очищать текущее содержимое буфера:
*/
function makeBuffer() {
  var text = '';

  function buffer(piece) {
    if (arguments.length == 0) { // вызов без аргументов
      return text;
    }
    text += piece;
  };

  buffer.clear = function() {
    text = '';
  }

  return buffer;
};

var buffer = makeBuffer();

buffer("Тест");
buffer(" тебя не съест ");
alert( buffer() ); // Тест тебя не съест

buffer.clear();

alert( buffer() ); // ""


/*
Сортировка

У нас есть массив объектов:
var users = [{
  name: "Вася",
  surname: 'Иванов',
  age: 20
}, {
  name: "Петя",
  surname: 'Чапаев',
  age: 25
}, {
  name: "Маша",
  surname: 'Медведева',
  age: 18
}];

Обычно сортировка по нужному полю происходит так:
// по полю name (Вася, Маша, Петя)
users.sort(function(a, b) {
  return a.name > b.name ? 1 : -1;
});

// по полю age  (Маша, Вася, Петя)
users.sort(function(a, b) {
  return a.age > b.age ? 1 : -1;
});

Мы хотели бы упростить синтаксис до одной строки, вот так:

users.sort(byField('name'));
users.forEach(function(user) {
  alert( user.name );
}); // Вася, Маша, Петя

users.sort(byField('age'));
users.forEach(function(user) {
  alert( user.name );
}); // Маша, Вася, Петя
То есть, вместо того, чтобы каждый раз писать в sort function... 
– будем использовать byField(...)

Напишите функцию byField(field), которую можно использовать в sort для 
сравнения объектов по полю field, чтобы пример выше заработал.
*/

function byField(field) {
  return function(a, b) {
    return a[field] > b[field] ? 1 : -1;
  }
}


/*
Фильтрация через функцию

Создайте функцию filter(arr, func), которая получает массив arr и возвращает 
новый, в который входят только те элементы arr, 
для которых func возвращает true.
Создайте набор «готовых фильтров»: inBetween(a,b) – «между a,b», inArray([...]) 
– "в массиве [...]". Использование должно быть таким:
filter(arr, inBetween(3,6)) – выберет только числа от 3 до 6,
filter(arr, inArray([1,2,3])) – выберет только элементы, совпадающие с одним из 
значений массива.
Пример, как это должно работать:
 .. ваш код для filter, inBetween, inArray ...
*/

function filter(arr, func) {
  var result = [];

  for (var i = 0; i < arr.length; i++) {
    var val = arr[i];
    if (func(val)) {
      result.push(val);
    }
  }

  return result;
}

function inBetween(a, b) {
    return function(x) {
      return x >= a && x <= b;
    };
  }

function inArray(arr) {
  return function(x) {
    return arr.indexOf(x) != -1;
  };
}

var arr = [1, 2, 3, 4, 5, 6, 7];

alert(filter(arr, function(a) {
  return a % 2 == 0
})); // 2,4,6

alert( filter(arr, inBetween(3, 6)) ); // 3,4,5,6

alert( filter(arr, inArray([1, 2, 10])) ); // 1,2


/*
Армия функций

Следующий код создает массив функций-стрелков shooters. По замыслу, каждый 
стрелок должен выводить свой номер:
function makeArmy() {
  var shooters = [];
  for (var i = 0; i < 10; i++) {
    var shooter = function() { // функция-стрелок
      alert( i ); // выводит свой номер
    };
    shooters.push(shooter);
  }
  return shooters;
}

var army = makeArmy();

army[0](); // стрелок выводит 10, а должен 0
army[5](); // стрелок выводит 10...
// .. все стрелки выводят 10 вместо 0,1,2...9
Почему все стрелки́ выводят одно и то же? Поправьте код, чтобы стрелки работали 
как задумано. Предложите несколько вариантов исправления.
*/
// 1. привязать значение непосредственно к функции-стрелку:
var shooter = function me() {
  alert( me.i );
};
shooter.i = i;

// 2. использовать дополнительную функцию для того, 
//чтобы «поймать» текущее значение i
var shooter = (function(x) {
  return function() {
    alert( x );
  };

})(i);

// 3. обернуть весь цикл во временную функцию:
for (var i = 0; i < 10; i++)(function(i) {
  var shooter = function() {
    alert( i );
  };
  shooters.push(shooter);
})(i);


/*
5. Модули через замыкания
6. Управление памятью в JavaScript
7. Устаревшая конструкция "with"
*/
/*
With + функция

Какая из функций будет вызвана?
*/
function f() {
  alert(1)
}

var obj = {
  f: function() {
    alert(2)
  }
};

with(obj) {
  f();        // 2
}

/*
With + переменные

Что выведет этот код?
*/
var a = 1;

var obj = {
  b: 2
};

with(obj) {
  var b;
  alert( a + b );     // 3
}





/****************************************************************************
* МЕТОДЫ ОБЪЕКТОВ И КОНТЕКСТ ВЫЗОВА
****************************************************************************/
/*
1. Методы объектов, this

Вызов в контексте массива
Каким будет результат? Почему?
*/
var arr = ["a", "b"];

arr.push(function() {
  alert( this );
})

arr[2](); // ? a,b,function...


/*
Проверка синтаксиса

Каков будет результат этого кода?
*/
var obj = {
  go: function() { alert(this) }
}

(obj.go)() //error...        var obj = { go:... }(obj.go)()
//P.S. Есть подвох :)


/*
Почему this присваивается именно так?

Вызовы (1) и (2) в примере ниже работают не так, как (3) и (4):
*/
"use strict"

var obj, method;

obj = {
  go: function() { alert(this); }
};

obj.go();                    // (1) object
(obj.go)();                  // (2) object
(method = obj.go)();        // (3) undefined
(obj.go || obj.stop)();     // (4) undefined


/*
Значение this в объявлении объекта

Что выведет alert в этом коде? Почему?
*/
var user = {
  firstName: "Василий",

  export: this
};

alert( user.export.firstName ); // undefined


/*
Возврат this

Что выведет alert в этом коде? Почему?
*/
var name = "";

var user = {
  name: "Василий",

  export: function() {
    return this;
  }

};

alert( user.export().name ); // Василий


/*
Возврат объекта с this

Что выведет alert в этом коде? Почему?
*/
var name = "";

var user = {
  name: "Василий",

  export: function() {
    return {
      value: this
    };
  }

};

alert( user.export().value.name ); // Василий
alert( user.export().value == user ); // true


/*
Создайте калькулятор

Создайте объект calculator с тремя методами:

read() запрашивает prompt два значения и сохраняет их как свойства объекта
sum() возвращает сумму этих двух значений
mul() возвращает произведение этих двух значений
*/
var calculator = {
  a: 0,
  b: 0,
  
  read: function() {
    this.a = +prompt("Введите первое значение:");
    this.b = +prompt("Введите второе значение:");
  },

  sum: function() {
    return this.a + this.b;
  },
  
  mul: function() {
    return this.a * this.b;
  }
}

calculator.read();
alert( calculator.sum() );
alert( calculator.mul() );

// or
var calculator = {
  sum: function() {
    return this.a + this.b;
  },

  mul: function() {
    return this.a * this.b;
  },

  read: function() {
    this.a = +prompt('a?', 0);
    this.b = +prompt('b?', 0);
  }
}


/*
Цепочка вызовов

Есть объект «лестница» ladder:
*/
var ladder = {
  step: 0,
  up: function() { // вверх по лестнице
    this.step++;
  },
  down: function() { // вниз по лестнице
    this.step--;
  },
  showStep: function() { // вывести текущую ступеньку
    alert( this.step );
  }
};
//Сейчас, если нужно последовательно вызвать несколько методов объекта, 
//это можно сделать так:

ladder.up();
ladder.up();
ladder.down();
ladder.showStep(); // 1
//Модифицируйте код методов объекта, чтобы вызовы можно было делать цепочкой, 
//вот так:

ladder.up().up().down().up().down().showStep(); // 1
//Как видно, такая запись содержит «меньше букв» и может быть более наглядной.
//Такой подход называется «чейнинг» (chaining) и используется, например, 
//во фреймворке jQuery.

var ladder = {
  step: 0,
  up: function() {
    this.step++;
    return this;
  },
  down: function() {
    this.step--;
    return this;
  },
  showStep: function() {
    alert( this.step );
    return this;
  }
}


/*
2. Преобразование объектов: toString и valueOf

['x'] == 'x'
Почему результат true ?
alert( ['x'] == 'x' );
*/
alert( ['x', 'y'] == 'x,y' ); // true
alert( [] == '' ); // true
//В данном случае, элемент только один – он и возвращается. 
//Так что ['x'] становится 'x'. Получилось 'x' == 'x', верно.


/*
Преобразование

Объявлен объект с toString и valueOf.
Какими будут результаты alert?
*/
var foo = {
  toString: function() {
    return 'foo';
  },
  valueOf: function() {
    return 2;
  }
};

alert( foo );                       // foo
alert( foo + 1 );                   // 3
alert( foo + "3" );                 // 23
//Подумайте, прежде чем ответить.


/*
Почему [] == [] неверно, а [ ] == ![ ] верно?

Почему первое равенство – неверно, а второе – верно?
alert( [] == [] ); // false
alert( [] == ![] ); // true
Какие преобразования происходят при вычислении?
*/
alert( [] == [] ); // false
//Два объекта равны только тогда, когда это один и тот же объект.
//В первом равенстве создаются два массива, это разные объекты, 
//так что они неравны.

alert( [] == ![] ); // true
alert( [] == false );
alert( '' == false );
alert( 0 == 0 );

/*
Вопросник по преобразованиям, для объектов

Подумайте, какой результат будет у выражений ниже. Когда закончите – сверьтесь 
с решением.
*/
new Date(0) - 0         // 0
new Array(1)[0] + ""    // "undefined"
({})[0]                 // undefined
[1] + 1                 // "11"
[1,2] + [3,4]           // "1,23,4"
[] + null + 1           // "null1"
[[0]][0][0]             // 0
({} + {})               // [object Object][object Object]


/*
Сумма произвольного количества скобок

Напишите функцию sum, которая будет работать так:
sum(1)(2) == 3; // 1 + 2
sum(1)(2)(3) == 6; // 1 + 2 + 3
sum(5)(-1)(2) == 6
sum(6)(-1)(-2)(-3) == 0
sum(0)(1)(2)(3)(4)(5) == 15
Количество скобок может быть любым.
Пример такой функции для двух аргументов – есть в решении задачи 
Сумма через замыкание.
*/

function sum(a) {
  var currentSum = a;
  function f(b) {
    currentSum += b;
    return f;
  }
  
  f.toString = function () {
      return currentSum;
  }
  
  return f;
}


/*
3. Создание объектов через "new"

Две функции один объект
Возможны ли такие функции A и B в примере ниже, что соответствующие объекты a,b 
равны (см. код ниже)?
function A() { ... }
function B() { ... }
var a = new A;
var b = new B;
alert( a == b ); // true
Если да – приведите пример кода с такими функциями.
*/

var obj = {};
function A() { return obj; }
function B() { return obj; }


/*
Создать Calculator при помощи конструктора

Напишите функцию-конструктор Calculator, которая создает объект 
с тремя методами:
Метод read() запрашивает два значения при помощи prompt 
и запоминает их в свойствах объекта.
Метод sum() возвращает сумму запомненных свойств.
Метод mul() возвращает произведение запомненных свойств.
Пример использования:
var calculator = new Calculator();
calculator.read();
alert( "Сумма=" + calculator.sum() );
alert( "Произведение=" + calculator.mul() );
*/
function Calculator() {
  this.read = function () {
    this.a = +prompt('a+',0);
    this.b = +prompt('b+',0);  
  }
  
  this.sum = function () {
    return this.a + this.b;
  }
  
  this.mul = function () {
    return this.a * this.b;
  }
}


/*
Создать Accumulator при помощи конструктора

Напишите функцию-конструктор Accumulator(startingValue). Объекты, которые она 
создает, должны хранить текущую сумму и прибавлять к ней то, 
что вводит посетитель.
Более формально, объект должен:
Хранить текущее значение в своём свойстве value. Начальное значение свойства 
value ставится конструктором равным startingValue.
Метод read() вызывает prompt, принимает число и прибавляет его к свойству value.
Таким образом, свойство value является текущей суммой всего, что ввел посетитель
при вызовах метода read(), с учетом начального значения startingValue.

Ниже вы можете посмотреть работу кода:

var accumulator = new Accumulator(1); // начальное значение 1
accumulator.read(); // прибавит ввод prompt к текущему значению
accumulator.read(); // прибавит ввод prompt к текущему значению
alert( accumulator.value ); // выведет текущее значение
*/
function Accumulator(startingValue) {
  this.value = startingValue;
    
  this.read = function () {
    this.value += +prompt('value+',0);
  }
}


/*
Создайте калькулятор

Напишите конструктор Calculator, который создаёт расширяемые 
объекты-калькуляторы.
Эта задача состоит из двух частей, которые можно решать одна за другой.
Первый шаг задачи: вызов calculate(str) принимает строку, например «1 + 2», 
с жёстко заданным форматом «ЧИСЛО операция ЧИСЛО» (по одному пробелу вокруг 
операции), и возвращает результат. Понимает плюс + и минус -.
Пример использования:
var calc = new Calculator;
alert( calc.calculate("3 + 7") ); // 10

Второй шаг – добавить калькулятору метод addMethod(name, func), который учит 
калькулятор новой операции. Он получает имя операции name и функцию от двух 
аргументов func(a,b), которая должна её реализовывать.

Например, добавим операции умножить *, поделить / и возвести в степень **:
var powerCalc = new Calculator;
powerCalc.addMethod("*", function(a, b) {
  return a * b;
});
powerCalc.addMethod("/", function(a, b) {
  return a / b;
});
powerCalc.addMethod("**", function(a, b) {
  return Math.pow(a, b);
});

var result = powerCalc.calculate("2 ** 3");
alert( result ); // 8
Поддержка скобок и сложных математических выражений в этой задаче не требуется.
Числа и операции могут состоять из нескольких символов. 
Между ними ровно один пробел.
Предусмотрите обработку ошибок. Какая она должна быть – решите сами.
*/

function Calculator() {

  var methods = {
    "-": function(a, b) {
      return a - b;
    },
    "+": function(a, b) {
      return a + b;
    }
  };

  this.calculate = function(str) {

    var split = str.split(' '),
      a = +split[0],
      op = split[1],
      b = +split[2]

    if (!methods[op] || isNaN(a) || isNaN(b)) {
      return NaN;
    }

    return methods[op](a, b);
  }

  this.addMethod = function(name, func) {
    methods[name] = func;
  };
}


/*
4. Дескрипторы, геттеры и сеттеры свойств

Добавить get/set-свойства
Вам попал в руки код объекта User, который хранит имя и фамилию в свойстве 
this.fullName:
function User(fullName) {
  this.fullName = fullName;
}
var vasya = new User("Василий Попкин");
Имя и фамилия всегда разделяются пробелом.
Сделайте, чтобы были доступны свойства firstName и lastName, причём не только 
на чтение, но и на запись, вот так:
var vasya = new User("Василий Попкин");
// чтение firstName/lastName
alert( vasya.firstName ); // Василий
alert( vasya.lastName ); // Попкин
// запись в lastName
vasya.lastName = 'Сидоров';
alert( vasya.fullName ); // Василий Сидоров
Важно: в этой задаче fullName должно остаться свойством, а firstName/lastName – 
реализованы через get/set. Лишнее дублирование здесь ни к чему.
*/
function User(fullName) {
  this.fullName = fullName;

  Object.defineProperties(this, {

    firstName: {

      get: function() {
        return this.fullName.split(' ')[0];
      },

      set: function(newFirstName) {
        this.fullName = newFirstName + ' ' + this.lastName;
      }

    },

    lastName: {

      get: function() {
        return this.fullName.split(' ')[1];
      },

      set: function(newLastName) {
        this.fullName = this.firstName + ' ' + newLastName;
      }

    }

  });
}


/*
5. Статические и фабричные методы

Счетчик объектов
Добавить в конструктор Article:
Подсчёт общего количества созданных объектов.
Запоминание даты последнего созданного объекта.
Используйте для этого статические свойства.
Пусть вызов Article.showStats() выводит то и другое.
Использование:
*/
function Article() {
  this.created = new Date();

  Article.count++; // увеличиваем счетчик при каждом вызове
  Article.last = this.created; // и запоминаем дату
}
Article.count = 0; // начальное значение
// (нельзя оставить undefined, т.к. Article.count++ будет NaN)

Article.showStats = function() {
  alert( 'Всего: ' + this.count + ', Последняя: ' + this.last );
};

new Article();
new Article();

Article.showStats(); // Всего: 2, Последняя: (дата)

new Article();

Article.showStats(); // Всего: 3, Последняя: (дата)


/*
6. Явное указание this: "call", "apply"

Перепишите суммирование аргументов
Есть функция sum, которая суммирует все элементы массива:
function sum(arr) {
  return arr.reduce(function(a, b) {
    return a + b;
  });
}

alert( sum([1, 2, 3]) ); // 6 (=1+2+3)
Создайте аналогичную функцию sumArgs(), которая будет суммировать все свои 
аргументы:
function sumArgs() {
  / ваш код /
}

alert( sumArgs(1, 2, 3) ); // 6, аргументы переданы через запятую, без массива
Для решения примените метод reduce к arguments, используя call, apply или 
одалживание метода.
P.S. Функция sum вам не понадобится, она приведена в качестве примера 
использования reduce для похожей задачи.
*/

function sumArgs() {
  arguments.reduce = [].reduce;
  return arguments.reduce(function(a,b) {
    return a + b;
  });
}
// or
function sumArgs() {
  return [].reduce.call(arguments, function(a, b) {
    return a + b;
  });
}


/*
Примените функцию к аргументам

Напишите функцию applyAll(func, arg1, arg2...), которая получает функцию func и 
произвольное количество аргументов.
Она должна вызвать func(arg1, arg2...), то есть передать в func все аргументы, 
начиная со второго, и возвратить результат.
Например:
// Применить Math.max к аргументам 2, -2, 3
alert( applyAll(Math.max, 2, -2, 3) ); // 3
// Применить Math.min к аргументам 2, -2, 3
alert( applyAll(Math.min, 2, -2, 3) ); // -2
Область применения applyAll, конечно, шире, 
можно вызывать её и со своими функциями:
function sum() { // суммирует аргументы: sum(1,2,3) = 6
  return [].reduce.call(arguments, function(a, b) {
    return a + b;
  });
}
function mul() { // перемножает аргументы: mul(2,3,4) = 24
  return [].reduce.call(arguments, function(a, b) {
    return a * b;
  });
}
alert( applyAll(sum, 1, 2, 3) ); // -> sum(1, 2, 3) = 6
alert( applyAll(mul, 2, 3, 4) ); // -> mul(2, 3, 4) = 24
*/

function sum() {
  return [].reduce.call(arguments, function(a, b) {
    return a + b;
  });
}

function mul() {
  return [].reduce.call(arguments, function(a, b) {
    return a * b;
  });
}

function applyAll(func) {
    return func.apply(this, [].slice.call(arguments, 1));
}


/*
7. Привязка контекста и карринг: "bind"

Кросс-браузерная эмуляция bind
Если вы вдруг захотите копнуть поглубже – аналог bind для IE8- и старых версий 
других браузеров будет выглядеть следующим образом:
function bind(func, context , ...args...) {
  var bindArgs = [].slice.call(arguments, 2); // (1)
  function wrapper() {                        // (2)
    var args = [].slice.call(arguments);
    var unshiftArgs = bindArgs.concat(args);  // (3)
    return func.apply(context, unshiftArgs);  // (4)
  }
  return wrapper;
}
Использование – вместо mul.bind(null, 2) вызывать bind(mul, null, 2).

Не факт, что он вам понадобится, но в качестве упражнения попробуйте 
разобраться, как это работает.
*/

//1. Вызов bind сохраняет дополнительные аргументы args (они идут со 2-го номера) 
//в массив bindArgs.
//2. … и возвращает обертку wrapper.
//3. Эта обёртка делает из arguments массив args и затем, используя метод 
//concat, прибавляет их к аргументам bindArgs (карринг).
//4. Затем передаёт вызов func с контекстом и общим массивом аргументов.


/*
Запись в объект после bind

Что выведет функция?
*/
function f() {
  alert( this );
}

var user = {
  g: f.bind("Hello")
}

user.g(); // Hello


/*
Повторный bind

Что выведет этот код?
*/
function f() {
  alert(this.name);
}

f = f.bind( {name: "Вася"} ).bind( {name: "Петя" } );

f(); // Вася


/*
Свойство функции после bind

В свойство функции записано значение. Изменится ли оно после применения bind? 
Обоснуйте ответ.
*/
function sayHi() {
  alert( this.name );
}
sayHi.test = 5;
alert( sayHi.test ); // 5

var bound = sayHi.bind({
  name: "Вася"
});

alert( bound.test ); // что выведет? почему?
// undefined.
// Результатом работы bind является функция-обёртка над sayHi. 
// Эта функция – самостоятельный объект, у неё уже нет свойства test.


/*
Использование функции вопросов

Вызов user.checkPassword() в коде ниже должен, при помощи ask, спрашивать пароль
и вызывать loginOk/loginFail в зависимости от правильности ответа.
Однако, его вызов приводит к ошибке. Почему?
Исправьте выделенную строку, чтобы всё работало (других строк изменять не надо).
*/
"use strict";

function ask(question, answer, ok, fail) {
  var result = prompt(question, '');
  if (result.toLowerCase() == answer.toLowerCase()) ok();
  else fail();
}

var user = {
  login: 'Василий',
  password: '12345',

  loginOk: function() {
    alert( this.login + ' вошёл в сайт' );
  },

  loginFail: function() {
    alert( this.login + ': ошибка входа' );
  },

  checkPassword: function() {
    //ask("Ваш пароль?", this.password, this.loginOk, this.loginFail);
    // 1.
    ask("Ваш пароль?", this.password, this.loginOk.bind(this), 
    this.loginFail.bind(this));
    
    //
  }
};

user.checkPassword();
//P.S. Ваше решение должно также срабатывать, если переменная user будет 
//перезаписана, например вместо user.checkPassword() в конце будут строки:

var vasya = user;
user = null;
vasya.checkPassword();

// or
//Решение через замыкание
//Альтернативное решение – сделать функции-обёртки 
//над user.loginOk/loginFail:
var user = {
  ...
  checkPassword: function() {
    ask("Ваш пароль?", this.password,
      function() { user.loginOk(); }, function() { user.loginFail(); });
  }
}
//…Но такой код использует переменную user, так что если объект переместить из 
//неё, к примеру, так, то работать он не будет:

var vasya = user; // переместим user в vasya
user = null;
vasya.checkPassword(); // упс будет ошибка, ведь в коде объекта остался user
//Для того, чтобы избежать проблем, можно использовать this. 
//Внутри checkPassword он всегда будет равен текущему объекту, 
//так что скопируем его в переменную, которую назовём self:
"use strict";

function ask(question, answer, ok, fail) {
  var result = prompt(question, '');
  if (result.toLowerCase() == answer.toLowerCase()) ok();
  else fail();
}

var user = {
  login: 'Василий',
  password: '12345',

  loginOk: function() {
    alert( this.login + ' вошёл в сайт' );
  },

  loginFail: function() {
    alert( this.login + ': ошибка входа' );
  },

  checkPassword: function() {
    var self = this;
    ask("Ваш пароль?", this.password,
      function() {
        self.loginOk();
      },
      function() {
        self.loginFail();
      }
    );
  }
};

var vasya = user;
user = null;
vasya.checkPassword();
//Теперь всё работает. Анонимные функции достают правильный контекст из 
//замыкания, где он сохранён в переменной self.


/*
Использование функции вопросов с каррингом

Эта задача – усложнённый вариант задачи Использование функции вопросов. 
В ней объект user изменён.
Теперь заменим две функции user.loginOk() и user.loginFail() на единый метод: 
user.loginDone(true/false), который нужно вызвать с true при верном ответе 
и с false – при неверном.
Код ниже делает это, соответствующий фрагмент выделен.
Сейчас он обладает важным недостатком: при записи в user другого значения 
объект перестанет корректно работать, вы увидите это, запустив пример ниже 
(будет ошибка).
Как бы вы написали правильно?
Исправьте выделенный фрагмент, чтобы код заработал.
*/
"use strict";

function ask(question, answer, ok, fail) {
  var result = prompt(question, '');
  if (result.toLowerCase() == answer.toLowerCase()) ok();
  else fail();
}

var user = {
  login: 'Василий',
  password: '12345',

  // метод для вызова из ask
  loginDone: function(result) {
    alert( this.login + (result ? ' вошёл в сайт' : ' ошибка входа') );
  },

  checkPassword: function() {
    /*
    ask("Ваш пароль?", this.password,
      function() {
        user.loginDone(true);
      },
      function() {
        user.loginDone(false);
      }
    );
    */
    //1. Решение с bind
    ask("Ваш пароль?", this.password, this.loginDone.bind(this, true), 
    this.loginDone.bind(this, false));
    
    // 2. Решение с локальной переменной
    var self = this;
    ask("Ваш пароль?", this.password,
      function() {
        self.loginDone(true);
      },
      function() {
        self.loginDone(false);
      }
    );
  }
};

var vasya = user;
user = null;
vasya.checkPassword();
//Изменения должны касаться только выделенного фрагмента.
//Если возможно, предложите два решения, одно – с использованием bind, 
//другое – без него. Какое решение лучше?


/*
8. Функции-обёртки, декораторы

Логирующий декоратор (1 аргумент)

Создайте декоратор makeLogging(f, log), который берет функцию f и массив log.
Он должен возвращать обёртку вокруг f, которая при каждом вызове записывает 
(«логирует») аргументы в log, а затем передает вызов в f.
В этой задаче можно считать, что у функции f ровно один аргумент.
Работать должно так:
*/

function work(a) {
  // work - произвольная функция, один аргумент
}

function makeLogging(f, log) {
  function wrapper(a) {
    log.push(a);
    return f.call(this, a);
  }

  return wrapper;
}

var log = [];
work = makeLogging(work, log);

work(1); // 1, добавлено в log
work(5); // 5, добавлено в log

for (var i = 0; i < log.length; i++) {
  alert( 'Лог:' + log[i] ); // "Лог:1", затем "Лог:5"
}


/*
Логирующий декоратор (много аргументов)

Создайте декоратор makeLogging(func, log), для функции func возвращающий 
обёртку, которая при каждом вызове добавляет её аргументы в массив log.
Условие аналогично задаче Логирующий декоратор (1 аргумент), но допускается func
 с любым набором аргументов.
Работать должно так:
*/
function work(a, b) {
  alert( a + b ); // work - произвольная функция
}

function makeLogging(f, log) { 
  /* ваш код */ 
  function wrapper() {
    log.push([].slice.call(arguments));
    return f.apply(this, arguments);
  }

  return wrapper;
}

var log = [];
work = makeLogging(work, log);

work(1, 2); // 3
work(4, 5); // 9

for (var i = 0; i < log.length; i++) {
  var args = log[i]; // массив из аргументов i-го вызова
  alert( 'Лог:' + args.join() ); // "Лог:1,2", "Лог:4,5"
}


/*
Кеширующий декоратор

Создайте декоратор makeCaching(f), который берет функцию f и возвращает обертку,
которая кеширует её результаты.
В этой задаче функция f имеет только один аргумент, и он является числом.
При первом вызове обертки с определенным аргументом – она вызывает f и 
запоминает значение.
При втором и последующих вызовах с тем же аргументом возвращается запомненное 
значение.
Должно работать так:
*/

function f(x) {
  return Math.random() * x; // random для удобства тестирования
}

function makeCaching(f) {
  var cache = {};
  return function(x) {
    if (!(x in cache)) {
      cache[x] = f.call(this, x);
    }
    return cache[x];
  };
}

f = makeCaching(f);

var a, b;

a = f(1);
b = f(1);
alert( a == b ); // true (значение закешировано)

b = f(2);
alert( a == b ); // false, другой аргумент => другое значение





/****************************************************************************
* НЕКОТОРЫЕ ДРУГИЕ ВОЗМОЖНОСТИ
****************************************************************************/
/*
1. Типы данных: [[Class]], instanceof и утки

Полиморфная функция formatDate

Напишите функцию formatDate(date), которая возвращает дату в формате dd.mm.yy.
Ее первый аргумент должен содержать дату в одном из видов:
Как объект Date.
Как строку, например yyyy-mm-dd или другую в стандартном формате даты.
Как число секунд с 01.01.1970.
Как массив [гггг, мм, дд], месяц начинается с нуля
Для этого вам понадобится определить тип данных аргумента и, при необходимости, 
преобразовать входные данные в нужный формат.
Пример работы:
*/
function formatDate(date) {
  if (typeof date == 'number') {
    // перевести секунды в миллисекунды и преобразовать к Date
    date = new Date(date * 1000);
  } else if (typeof date == 'string') {
    // строка в стандартном формате автоматически будет разобрана в дату
    date = new Date(date); 
  } else if (Array.isArray(date)) { 
    date = new Date(date[0], date[1], date[2]);
  }
  // преобразования для поддержки полиморфизма завершены, 
  // теперь мы работаем с датой (форматируем её)

  return date.toLocaleString("ru", 
    {day: '2-digit', month: '2-digit', year: '2-digit'});

  /*
  // можно и вручную, если лень добавлять в старый IE поддержку локализации
  var day = date.getDate();
  if (day < 10) day = '0' + day;

  var month = date.getMonth() + 1;
  if (month < 10) month = '0' + month;

  // взять 2 последние цифры года
  var year = date.getFullYear() % 100;
  if (year < 10) year = '0' + year;

  var formattedDate = day + '.' + month + '.' + year;
  
  return formattedDate;
  */
}

alert( formatDate('2011-10-02') ); // 02.10.11
alert( formatDate(1234567890) ); // 14.02.09
alert( formatDate([2014, 0, 1]) ); // 01.01.14
alert( formatDate(new Date(2014, 0, 1)) ); // 01.01.14


/*
2. Формат JSON, метод toJSON

Превратите объект в JSON
Превратите объект leader из примера ниже в JSON:
*/
var leader = {
  name: "Василий Иванович",
  age: 35
};

var str = JSON.stringify(leader); 
alert(str);
//После этого прочитайте получившуюся строку обратно в объект.
var event = JSON.parse(str);
alert(event);


/*
Превратите объекты со ссылками в JSON

Превратите объект team из примера ниже в JSON:
*/
var leader = {
  name: "Василий Иванович"
};

var soldier = {
  name: "Петька"
};

var leaderStr = JSON.stringify(leader); 
var soldierStr = JSON.stringify(soldier); 

// эти объекты ссылаются друг на друга!
leader.soldier = soldier;
soldier.leader = leader;

var team = [leader, soldier];
//Может ли это сделать прямой вызов JSON.stringify(team)? 
//Если нет, то почему?
// 
//Обычный вызов JSON.stringify(team) выдаст ошибку, так как объекты leader и 
//soldier внутри структуры ссылаются друг на друга.
//Формат JSON не предусматривает средств для хранения ссылок.

//Какой подход вы бы предложили для чтения и восстановления таких 
//объектов?

//1. Добавить в team свой код toJSON:
team.toJSON = function() {
  /* свой код, который может создавать копию объекта без круговых ссылок и 
  передавать управление JSON.stringify */
}
//При этом, конечно, понадобится и своя функция чтения из JSON, 
//которая будет восстанавливать объект, азатем дополнять его круговыми ссылками.

// 2. Можно учесть возможную проблему в самой структуре, 
//используя вместо ссылок id
var leader = {
  id: 12,
  name: "Василий Иванович"
};

var soldier = {
  id: 51,
  name: "Петька"
};

// поменяли прямую ссылку на ID
leader.soldierId = 51;
soldier.leaderId = 12;

var team = {
  12: leader,
  51: soldier
};


/*
3. setTimeout и setInterval

Вывод чисел каждые 100 мс

Напишите функцию printNumbersInterval(), которая последовательно выводит в 
консоль числа от 1 до 20, с интервалом между числами 100 мс. То есть, весь 
вывод должен занимать 2000 мс, в течение которых каждые 100 мс в консоли 
появляется очередное число.
Нажмите на кнопку, открыв консоль, для демонстрации:
printNumbersInterval()
P.S. Функция должна использовать setInterval.
*/
function printNumbersInterval() {
  var i = 1;
  var timerId = setInterval(function() {
    console.log(i);
    if (i == 20) clearInterval(timerId);
    i++;
  }, 100);
}

printNumbersInterval();


/*
Вывод чисел каждые 100 мс, через setTimeout

Сделайте то же самое, что в задаче Вывод чисел каждые 100 мс, но с 
использованием рекурсивного setTimeout вместо setInterval.
*/
function printNumbersTimeout20_100() {
  var i = 1;
  var timerId = setTimeout(function go() {
    console.log(i);
    if (i < 20) setTimeout(go, 100);
    i++;
  }, 100);
}

printNumbersTimeout20_100();


/*
Для подсветки setInterval или setTimeout?

Стоит задача: реализовать подсветку синтаксиса в длинном коде при помощи 
JavaScript, для онлайн-редактора кода. Это требует сложных вычислений, 
особенно загружает процессор генерация дополнительных элементов страницы, 
визуально осуществляющих подсветку.
Поэтому решаем обрабатывать не весь код сразу, что привело бы к зависанию 
скрипта, а разбить работу на части: подсвечивать по 20 строк раз в 10 мс.
Как мы знаем, есть два варианта реализации такой подсветки:
Через setInterval, с остановкой по окончании работы:
*/
timer = setInterval(function() {
  if (/*есть еще что подсветить*/) highlight();
  else clearInterval(timer);
}, 10);

//Через рекурсивный setTimeout:

setTimeout(function go() {
  highlight();
  if (/*есть еще что подсветить*/) setTimeout(go, 10);
}, 10);
//Какой из них стоит использовать? Почему?

//Нужно выбрать вариант 2, который гарантирует браузеру свободное время между 
//выполнениями highlight.


/*
Что выведет setTimeout?

В коде ниже запланирован запуск setTimeout, а затем запущена тяжёлая функция 
hardWork, выполнение которой занимает более долгое время, чем интервал до 
срабатывания таймера.
Когда сработает setTimeout? Выберите нужный вариант:
До выполнения hardWork.
Во время выполнения hardWork.
Сразу же по окончании hardWork.
Через 100 мс после окончания hardWork.
Что выведет alert в коде ниже?
*/
setTimeout(function() {
  alert( i );
}, 100);

var i;

function hardWork() {
  // время выполнения этого кода >100 мс, сам код неважен
  for (i = 0; i < 1e8; i++) hardWork[i % 2] = i;
}

hardWork();

// Сразу же по окончании hardWork. --- все остальные браузеры
// Что выведет alert в коде ниже? i = 1e8 = 100000000


/*
Что выведет после setInterval?

В коде ниже запускается setInterval каждые 10 мс, и через 50 мс запланирована 
его отмена.
После этого запущена тяжёлая функция f, выполнение которой (мы точно знаем) 
потребует более 100 мс.
Сработает ли setInterval, как и когда?

Варианты:
Да, несколько раз, в процессе выполнения f.
Да, несколько раз, сразу после выполнения f.
Да, один раз, сразу после выполнения f.
Нет, не сработает.
Может быть по-разному, как повезёт.
Что выведет alert в строке (*)?
*/

var i;
var timer = setInterval(function() { // планируем setInterval каждые 10 мс
  i++;
}, 10);

setTimeout(function() { // через 50 мс - отмена setInterval
  clearInterval(timer);
  alert( i ); // (*)
}, 50);

// и запускаем тяжёлую функцию
function f() {
  // точное время выполнения не играет роли
  // здесь оно заведомо больше 100 мс
  for (i = 0; i < 1e8; i++) f[i % 2] = i;
}

f();
//Вызов alert(i) в setTimeout введет 100000001.
//Правильный вариант срабатывания: 3 (сразу же по окончании f один раз).


/*
Кто быстрее?

Есть два бегуна:
var runner1 = new Runner();
var runner2 = new Runner();
У каждого есть метод step(), который делает шаг, увеличивая свойство steps.
Конкретный код метода step() не имеет значения, важно лишь что шаг делается не 
мгновенно, он требует небольшого времени.
Если запустить первого бегуна через setInterval, а второго – через вложенный 
setTimeout – какой сделает больше шагов за 5 секунд?
*/
// первый?
setInterval(function() {
  runner1.step();
}, 15);

// или второй?
setTimeout(function go() {
  runner2.step();
  setTimeout(go, 15);
}, 15);

setTimeout(function() {
  alert( runner1.steps );
  alert( runner2.steps );
}, 5000);

//Есть браузеры, в которых на время работы JavaScript таймер «застывает», 
//например таков IE. В них количество шагов будет почти одинаковым, ±1.
//В других браузерах (Chrome) первый бегун будет быстрее.


/*
Функция-задержка

Напишите функцию delay(f, ms), которая возвращает обёртку вокруг f, 
задерживающую вызов на ms миллисекунд.
Например:
*/
function f(x) {
  alert( x );
}

var f1000 = delay(f, 1000);
var f1500 = delay(f, 1500);

f1000("тест"); // выведет "тест" через 1000 миллисекунд
f1500("тест2"); // выведет "тест2" через 1500 миллисекунд
/*
Упрощённо можно сказать, что delay возвращает "задержанный на ms" вариант f.
В примере выше у функции только один аргумент, но delay должна быть 
универсальной: передавать любое количество аргументов и контекст this.
*/
function delay(f, ms) {
  return function() {
    var savedThis = this;
    var savedArgs = arguments;
    
    setTimeout(function ) {
      f.apply(savedThis, savedArgs);
    }, ms);
  }
}


/*
Вызов не чаще чем в N миллисекунд

Напишите функцию debounce(f, ms), которая возвращает обёртку, которая 
откладывает вызов f на ms миллисекунд.
«Лишние» вызовы перезаписывают предыдущие отложенные задания. Все аргументы и 
контекст – передаются.
Например:
*/
function f() { ... }

let f = debounce(f, 1000);

f(1); // вызов отложен на 1000 мс
f(2); // предыдущий отложенный вызов игнорируется, 
      // текущий (2) откладывается на 1000 мс
      // через 1 секунду будет выполнен вызов f(1)
setTimeout( function() { f(3) }, 1100); // через 1100 мс отложим вызов 
                                        // еще на 1000 мс
setTimeout( function() { f(4) }, 1200); // игнорируем вызов (3)
// через 2200 мс от начала выполнения будет выполнен вызов f(4)

//Упрощённо можно сказать, что debounce возвращает вариант f, срабатывающий не 
//чаще чем раз в ms миллисекунд.
function debounce(f, ms) {

  let timer = null;

  return function(...args) {
    const onComplete = () => {
      f.apply(this, args);
      timer =null;
    }

    if (timer) {
      clearTimeout(timer);
    }
    
    timer = setTimeout(onComplete, ms);
  };
}


/*
Тормозилка

Напишите функцию throttle(f, ms) – «тормозилку», которая возвращает обёртку, 
передающую вызов f не чаще, чем раз в ms миллисекунд.

У этой функции должно быть важное существенное отличие от debounce: если 
игнорируемый вызов оказался последним, т.е. после него до окончания задержки 
ничего нет – то он выполнится.
Чтобы лучше понять, откуда взялось это требование, и как throttle должна 
работать – разберём реальное применение, на которое и ориентирована эта задача.

Например, нужно обрабатывать передвижения мыши.
В JavaScript это делается функцией, которая будет запускаться при каждом 
микро-передвижении мыши и получать координаты курсора. По мере того, как 
мышь двигается, эта функция может запускаться очень часто, может быть 100 
раз в секунду (каждые 10 мс).

Функция обработки передвижения должна обновлять некую информацию на странице.
При этом обновление – слишком «тяжёлый» процесс, чтобы делать его при каждом 
микро-передвижении. Имеет смысл делать его раз в 100 мс, не чаще.
Пусть функция, которая осуществляет это обновление по передвижению, называется 
onmousemove.
Вызов throttle(onmousemove, 100), по сути, предназначен для того, чтобы 
«притормаживать» обработку onmousemove. Технически, он должен возвращать 
обёртку, которая передаёт все вызовы onmousemove, но не чаще чем раз в 100 мс.

При этом промежуточные движения можно игнорировать, но мышь в конце концов 
где-то остановится. И это последнее, итоговое положение мыши обязательно нужно 
обработать!
Визуально это даст следующую картину обработки перемещений мыши:
1. Первое обновление произойдёт сразу (это важно, посетитель тут же видит 
реакцию на своё действие).
2. Дальше может быть много вызовов (микро-передвижений) с разными координатами, 
но пока не пройдёт 100 мс – ничего не будет.
3. По истечении 100 мс – опять обновление, с последними координатами. 
Промежуточные микро-передвижения игнорированы.
4. В конце концов мышь где-то остановится, обновление по окончании очередной 
паузы 100 мс сработает с последними координатами.
Ещё раз заметим – задача из реальной жизни, и в ней принципиально важно, что 
последнее передвижение обрабатывается. Пользователь должен увидеть, где 
остановил мышь.

Пример использования:
*/
var f = function(a) {
  console.log(a)
};

// затормозить функцию до одного раза в 1000 мс
var f1000 = throttle(f, 1000);

f1000(1); // выведет 1
f1000(2); // (тормозим, не прошло 1000 мс)
f1000(3); // (тормозим, не прошло 1000 мс)

// когда пройдёт 1000 мс...
// выведет 3, промежуточное значение 2 игнорируется

function throttle(func, ms) {
  var isThrottled = false,
      savedArgs,
      savedThis;

  function wrapper() {
    if (isThrottled) {
      savedArgs = arguments;
      savedThis = this;
      return;
    }
    
    func.apply(this, arguments);
    
    isThrottled = true;
    
    setTimeout(function() {
        isThrottled = false;
        if (savedArgs) {
          wrapper.apply(savedThis, savedArgs);
          savedArgs = savedThis = null;
        }
    }, ms);
  }
  
  return wrapper;
}


/*
4. Запуск кода из строки: eval

Eval-калькулятор

Напишите интерфейс, который принимает математическое выражение (prompt) и 
возвращает его результат.
Проверять выражение на корректность не требуется.
*/
var str = prompt('введите выражение: ', '2 + 3 * 4');
alert(eval(str));


/*
5. Перехват ошибок, "try..catch"

Finally или просто код?

Сравните два фрагмента кода.
Первый использует finally для выполнения кода по выходу из try..catch:
*/
try {
  //начать работу
  //работать
} catch (e) {
  //обработать ошибку
} finally {
  //финализация: завершить работу
}
//Второй фрагмент просто ставит очистку ресурсов за try..catch:
try {
  //начать работу
} catch (e) {
  //обработать ошибку
}
//финализация: завершить работу
/*
Нужно, чтобы код финализации всегда выполнялся при выходе из блока try..catch и,
таким образом, заканчивал начатую работу. Имеет ли здесь finally какое-то 
преимущество или оба фрагмента работают одинаково?
Если имеет, то дайте пример когда код с finally работает верно, а без – неверно.
*/

//Разница в поведении станет очевидной, если рассмотреть код внутри функции.
//Поведение будет различным, если управление каким-то образом выпрыгнет 
//из try..catch.
//Например, finally сработает после return, но до передачи управления 
//внешнему коду:

function f() {
  try {
    ...
    return result; // !!!!!!
  } catch (e) {
    ...
  } finally {
    //очистить ресурсы
  }
}
//Или же управление может выпрыгнуть из-за throw:
function f() {
  try {
    ...

  } catch (e) {
    ...
    if(/*не умею обрабатывать эту ошибку*/) {
      throw e;
    }

  } finally {
    //очистить ресурсы
  }
}
//В этих случаях именно finally гарантирует выполнение кода до окончания работы 
//f, просто код не будет вызван.


/*
Eval-калькулятор с ошибками

Напишите интерфейс, который принимает математическое выражение (в prompt) и 
результат его вычисления через eval.
При ошибке нужно выводить сообщение и просить переввести выражение.
Ошибкой считается не только некорректное выражение, такое как 2+, 
но и выражение, возвращающее NaN, например 0/0.
*/
var str = prompt('введите выражение: ', '2 + 3 * 4');

try {
  var result = eval(str);
  if (isNaN(result)){
      throw new Error("Результат неопределён");
  }
  alert(result)
} catch (e) {
  alert('Ошибка!');
}

// or
var expr, res;

while (true) {
  expr = prompt("Введите выражение?", '2-');
  if (expr == null) break;

  try {
    res = eval(expr);
    if (isNaN(res)) {
      throw new Error("Результат неопределён");
    }

    break;
  } catch (e) {
    alert( "Ошибка: " + e.message + ", повторите ввод" );
  }
}

alert( res );





/****************************************************************************
* 7. ООП В ФУНКЦИОНАЛЬНОМ СТИЛЕ
****************************************************************************/
/*
1. Введение
2. Внутренний и внешний интерфейс

Добавить метод и свойство кофеварке
важность: 5
Улучшите готовый код кофеварки, который дан ниже: добавьте в кофеварку публичный
метод stop(), который будет останавливать кипячение (через clearTimeout).
*/
 function CoffeeMachine(power) {
  this.waterAmount = 0;

  var WATER_HEAT_CAPACITY = 4200;

  var self = this;

  function getBoilTime() {
    return self.waterAmount * WATER_HEAT_CAPACITY * 80 / power;
  }

  function onReady() {
    alert( 'Кофе готово!' );
  }

  this.run = function() {
    setTimeout(onReady, getBoilTime());
  };

}
//Вот такой код должен ничего не выводить:

var coffeeMachine = new CoffeeMachine(50000);
coffeeMachine.waterAmount = 200;

coffeeMachine.run();
coffeeMachine.stop(); // кофе приготовлен не будет
//P.S. Текущую температуру воды вычислять и хранить не требуется.
//P.P.S. При решении вам, скорее всего, понадобится добавить приватное свойство 
//timerId, которое будет хранить текущий таймер.

function CoffeeMachine(power) {
  this.waterAmount = 0;

  var WATER_HEAT_CAPACITY = 4200;
  var timerId; //!!!!!
  var self = this;

  function getBoilTime() {
    return self.waterAmount * WATER_HEAT_CAPACITY * 80 / power;
  }

  function onReady() {
    alert( 'Кофе готово!' );
  }

  this.run = function() {
    timerId = setTimeout(onReady, getBoilTime());  //!!!!!
  };

  this.stop = function() { //!!!!!
    clearTimeout(timerId)
  };
}

var coffeeMachine = new CoffeeMachine(50000);
coffeeMachine.waterAmount = 200;

coffeeMachine.run();
coffeeMachine.stop(); // кофе приготовлен не будет


/*
3. Геттеры и сеттеры

Написать объект с геттерами и сеттерами

Напишите конструктор User для создания объектов:

С приватными свойствами имя firstName и фамилия surname.
С сеттерами для этих свойств.
С геттером getFullName(), который возвращает полное имя.
Должен работать так:
*/
function User() {
  var firstName = '';
  var surname = '';

  this.setFirstName = function(name){
      firstName = name;
  };

  this.setSurname = function(name){
      surname = name;
  };

  this.getFullName = function() {
      return (firstName + " " + surname);
  }
}

var user = new User();
user.setFirstName("Петя");
user.setSurname("Иванов");

alert( user.getFullName() ); // Петя Иванов


/*
Добавить геттер для power

Добавьте кофеварке геттер для приватного свойства power, чтобы внешний код мог 
узнать мощность кофеварки.
Исходный код:
*/
function CoffeeMachine(power, capacity) {
  //...
  this.setWaterAmount = function(amount) {
    if (amount < 0) {
      throw new Error("Значение должно быть положительным");
    }
    if (amount > capacity) {
      throw new Error("Нельзя залить воды больше, чем " + capacity);
    }

    waterAmount = amount;
  };

  this.getWaterAmount = function() {
    return waterAmount;
  };

}
/*
Обратим внимание, что ситуация, когда у свойства power есть геттер, но нет 
сеттера – вполне обычна.
Здесь это означает, что мощность power можно указать лишь при создании кофеварки
и в дальнейшем её можно прочитать, но нельзя изменить.
*/

function CoffeeMachine(power, capacity) {
  //...
  var power = '';
  
  this.getPower = function() {
      return power;
  }
  
  this.setWaterAmount = function(amount) {
    if (amount < 0) {
      throw new Error("Значение должно быть положительным");
    }
    if (amount > capacity) {
      throw new Error("Нельзя залить воды больше, чем " + capacity);
    }

    waterAmount = amount;
  };

  this.getWaterAmount = function() {
    return waterAmount;
  };

}


/*
Добавить публичный метод кофеварке

Добавьте кофеварке публичный метод addWater(amount), который будет добавлять 
воду.
При этом, конечно же, должны происходить все необходимые проверки – на 
положительность и превышение ёмкости.
Исходный код:
*/
function CoffeeMachine(power, capacity) {
  var waterAmount = 0;

  var WATER_HEAT_CAPACITY = 4200;

  function getTimeToBoil() {
    return waterAmount * WATER_HEAT_CAPACITY * 80 / power;
  }

  this.setWaterAmount = function(amount) {
    if (amount < 0) {
      throw new Error("Значение должно быть положительным");
    }
    if (amount > capacity) {
      throw new Error("Нельзя залить больше, чем " + capacity);
    }

    waterAmount = amount;
  };

  function onReady() {
    alert( 'Кофе готов!' );
  }

  this.run = function() {
    setTimeout(onReady, getTimeToBoil());
  };
  
  this.addWater = function(amount) {    // !!!!!
    this.setWaterAmount(waterAmount + amount);
  };

}
//Вот такой код должен приводить к ошибке:

var coffeeMachine = new CoffeeMachine(100000, 400);
coffeeMachine.addWater(200);
coffeeMachine.addWater(100);
coffeeMachine.addWater(300); // Нельзя залить больше, чем 400
coffeeMachine.run();


/*
Создать сеттер для onReady

Обычно когда кофе готов, мы хотим что-то сделать, например выпить его.
Сейчас при готовности срабатывает функция onReady, но она жёстко задана в коде:
*/
function CoffeeMachine(power, capacity) {
  var waterAmount = 0;

  var WATER_HEAT_CAPACITY = 4200;

  function getTimeToBoil() {
    return waterAmount * WATER_HEAT_CAPACITY * 80 / power;
  }

  this.setWaterAmount = function(amount) {
    // ... проверки пропущены для краткости
    waterAmount = amount;
  };

  this.getWaterAmount = function(amount) {
    return waterAmount;
  };

  function onReady() {
      alert( 'Кофе готов!' );
    }

  this.run = function() {
    setTimeout(onReady, getTimeToBoil());
  };

}
/*Создайте сеттер setOnReady, чтобы код снаружи мог назначить свой onReady, 
вот так:*/
var coffeeMachine = new CoffeeMachine(20000, 500);
coffeeMachine.setWaterAmount(150);

coffeeMachine.setOnReady(function() {
  var amount = coffeeMachine.getWaterAmount();
  alert( 'Готов кофе: ' + amount + 'мл' ); // Кофе готов: 150 мл
});

coffeeMachine.run();
//P.S. Значение onReady по умолчанию должно быть таким же, как и раньше.
//P.P.S. Постарайтесь сделать так, чтобы setOnReady можно было вызвать не только
//до, но и после запуска кофеварки, то есть чтобы функцию onReady можно было 
//изменить в любой момент до её срабатывания.

  this.setOnReady = function(newOnReady) {
    onReady = newOnReady;
  };

  this.run = function() {
    setTimeout(function() {
      onReady();
    }, getTimeToBoil());
  };


/*
Добавить метод isRunning

Из внешнего кода мы хотели бы иметь возможность понять – запущена кофеварка 
или нет.
Для этого добавьте кофеварке публичный метод isRunning(), который будет 
возвращать true, если она запущена и false, если нет.
Нужно, чтобы такой код работал:
*/
var coffeeMachine = new CoffeeMachine(20000, 500);
coffeeMachine.setWaterAmount(100);

alert( 'До: ' + coffeeMachine.isRunning() ); // До: false

coffeeMachine.run();
alert( 'В процессе: ' + coffeeMachine.isRunning() ); // В процессе: true

coffeeMachine.setOnReady(function() {
  alert( "После: " + coffeeMachine.isRunning() ); // После: false
});
//Исходный код возьмите из решения предыдущей задачи.
  var status;

  this.run = function() {
    status = setTimeout(function() {
      status = null;
      onReady();
    }, getTimeToBoil());
  };
  
  this.isRunning = function() {
    return !!status;
  }


/*
4. Функциональное наследование
*/
/*
Запускать только при включённой кофеварке

В коде CoffeeMachine сделайте так, чтобы метод run выводил ошибку, если 
кофеварка выключена.
В итоге должен работать такой код:
*/
var coffeeMachine = new CoffeeMachine(10000);
coffeeMachine.run(); // ошибка, кофеварка выключена!
//А вот так – всё в порядке:
var coffeeMachine = new CoffeeMachine(10000);
coffeeMachine.enable();
coffeeMachine.run(); // ...Кофе готов!


function Machine(power) {
  this._enabled = false;

  var self = this;

  this.enable = function() {
    // используем внешнюю переменную вместо this
    self._enabled = true;
  };

  this.disable = function() {
    self._enabled = false;
  };

}

function CoffeeMachine(power) {
  Machine.apply(this, arguments);

  var waterAmount = 0;

  this.setWaterAmount = function(amount) {
    waterAmount = amount;
  };

  var parentEnable = this.enable;
  this.enable = function() {
      parentEnable(); // теперь можно вызывать как угодно, this не важен
      this.run();
    }

  function onReady() {
    alert( 'Кофе готово!' );
  }

  this.run = function() {
    if(!this._enabled){
      throw new Error('ошибка, кофеварка выключена!');
    }
    
    setTimeout(onReady, 1000);
  };
}


/*
Останавливать кофеварку при выключении

Когда кофеварку выключают – текущая варка кофе должна останавливаться.
Например, следующий код кофе не сварит:
*/
var coffeeMachine = new CoffeeMachine(10000);
coffeeMachine.enable();
coffeeMachine.run();
coffeeMachine.disable(); // остановит работу, ничего не выведет
//Реализуйте это на основе решения предыдущей задачи.

function Machine(power) {
  this._enabled = false;

  this.enable = function() {
    this._enabled = true;
  };

  this.disable = function() {
    this._enabled = false;
  };
}

function CoffeeMachine(power) {
  Machine.apply(this, arguments);

  var waterAmount = 0;
  var timerId;

  this.setWaterAmount = function(amount) {
    waterAmount = amount;
  };

  function onReady() {
    alert('Кофе готов!');
  }

  var parentDisable = this.disable; // !!!!!
  this.disable = function() {
    parentDisable.call(this);
    clearTimeout(timerId);
  }

  this.run = function() {
    if (!this._enabled) {
      throw new Error("Кофеварка выключена");
    }
    timerId = setTimeout(onReady, 1000);
  };

}


/*
Унаследуйте холодильник

Создайте класс для холодильника Fridge(power), наследующий от Machine, с 
приватным свойством food и методами addFood(...), getFood():

Приватное свойство food хранит массив еды.
Публичный метод addFood(item) добавляет в массив food новую еду, доступен вызов 
с несколькими аргументами addFood(item1, item2...) для добавления нескольких 
элементов сразу.
Если холодильник выключен, то добавить еду нельзя, будет ошибка.
Максимальное количество еды ограничено power/100, где power – мощность 
холодильника, указывается в конструкторе. 
При попытке добавить больше – будет ошибка
Публичный метод getFood() возвращает еду в виде массива, добавление или 
удаление элементов из которого не должно влиять на свойство food холодильника.
Код для проверки:
*/
var fridge = new Fridge(200);
fridge.addFood("котлета"); // ошибка, холодильник выключен
//Ещё код для проверки:
// создать холодильник мощностью 500 (не более 5 еды)
var fridge = new Fridge(500);
fridge.enable();
fridge.addFood("котлета");
fridge.addFood("сок", "зелень");
fridge.addFood("варенье", "пирог", "торт"); // ошибка, слишком много еды
//Код использования холодильника без ошибок:
var fridge = new Fridge(500);
fridge.enable();
fridge.addFood("котлета");
fridge.addFood("сок", "варенье");

var fridgeFood = fridge.getFood();
alert( fridgeFood ); // котлета, сок, варенье

// добавление элементов не влияет на еду в холодильнике
fridgeFood.push("вилка", "ложка");

alert( fridge.getFood() ); // внутри по-прежнему: котлета, сок, варенье
//Исходный код класса Machine, от которого нужно наследовать:

function Machine(power) {
  this._power = power;
  this._enabled = false;

  var self = this;

  this.enable = function() {
    self._enabled = true;
  };

  this.disable = function() {
    self._enabled = false;
  };
}

function Fridge(power){
  Machine.apply(this,arguments);

  var food = [];

  this.addFood = function() {
    if (!this._enabled) {
      throw new Error("Холодильник выключен");
    }
    if (food.length + arguments.length) > this._power/100) {
      throw new Error("ошибка, слишком много еды");
    }
    for (var i = 0; i < arguments.length; i++) {
      food.push(arguments[i]);
    }
  };
  
  this.getFood = function() {
    return food.slice(); // в новый массив, чтобы манипул. с ним не меняли food
  };
}


/*
Добавьте методы в холодильник

Добавьте в холодильник методы:
Публичный метод filterFood(func), который возвращает всю еду, для которой 
func(item) == true
Публичный метод removeFood(item), который удаляет еду item из холодильника.
Код для проверки:
*/
var fridge = new Fridge(500);
fridge.enable();
fridge.addFood({
  title: "котлета",
  calories: 100
});
fridge.addFood({
  title: "сок",
  calories: 30
});
fridge.addFood({
  title: "зелень",
  calories: 10
});
fridge.addFood({
  title: "варенье",
  calories: 150
});

fridge.removeFood("нет такой еды"); // без эффекта
alert( fridge.getFood().length ); // 4

var dietItems = fridge.filterFood(function(item) {
  return item.calories < 50;
});

dietItems.forEach(function(item) {
  alert( item.title ); // сок, зелень
  fridge.removeFood(item);
});

alert( fridge.getFood().length ); // 2

// РЕШЕНИЕ
function Machine(power) {
  this._power = power;
  this._enabled = false;

  var self = this;

  this.enable = function() {
    self._enabled = true;
  };

  this.disable = function() {
    self._enabled = false;
  };
}

function Fridge(power){
  Machine.apply(this,arguments);

  var food = [];

  this.addFood = function() {
    if (!this._enabled) {
      throw new Error("Холодильник выключен");
    }
    if (food.length + arguments.length) > this._power/100) {
      throw new Error("ошибка, слишком много еды");
    }
    for (var i = 0; i < arguments.length; i++) {
      food.push(arguments[i]);
    }
  };
  
  this.getFood = function() {
    return food.slice(); // в новый массив, чтобы манипул. с ним не меняли food
  };

  //Публичный метод filterFood(func), который возвращает всю еду, 
  //для которой func(item) == true
  this.filterFood = function(filter) {
    return food.filter(filter);
  };

  //Публичный метод removeFood(item), который удаляет еду item из холодильника.
  this.removeFood = function(item) {
    var idx = food.indexOf(item);
    if (idx != -1) food.splice(idx, 1)
  };
}


/*
Переопределите disable

Переопределите метод disable холодильника, чтобы при наличии в нём еды он 
выдавал ошибку.
Код для проверки:
*/
var fridge = new Fridge(500);
fridge.enable();
fridge.addFood("кус-кус");
fridge.disable(); // ошибка, в холодильнике есть еда

// ...
var parentDisable = this.disable;
this.disable = function() {
  if (food.length) {
    throw new Error("ошибка, в холодильнике есть еда");
  }
  parentDisable();
}
// ...




/****************************************************************************
* 8. ООП В ПРОТОТИПНОМ СТИЛЕ
****************************************************************************/
/*
1. Прототип объекта
*/
/*
Чему равно свойство после delete?

Какие значения будут выводиться в коде ниже?
*/
var animal = {
  jumps: null
};
var rabbit = {
  jumps: true
};

rabbit.__proto__ = animal;
alert( rabbit.jumps ); // ? (1) true
delete rabbit.jumps;
alert( rabbit.jumps ); // ? (2) null
delete animal.jumps;
alert( rabbit.jumps ); // ? (3) undefined
//Итого три вопроса.


/*
Прототип и this

Сработает ли вызов rabbit.eat() ?
Если да, то в какой именно объект он запишет свойство full: в rabbit или animal?
*/
var animal = {
  eat: function() {
    this.full = true;
  }
};

var rabbit = {
  __proto__: animal
};

rabbit.eat();
// yes
// rabbit


/*
Алгоритм для поиска

Есть объекты:
*/
var head = {
  glasses: 1
};

var table = {
  pen: 3
};

var bed = {
  sheet: 1,
  pillow: 2
};

var pockets = {
  money: 2000
};
/*
Задание состоит из двух частей:
Присвойте объектам ссылки __proto__ так, чтобы любой поиск чего-либо шёл по 
алгоритму pockets -> bed -> table -> head.
*/
pockets.__proto__ = bed;
bed.__proto__ = table;
table.__proto__ = head;
/*То есть pockets.pen == 3, bed.glasses == 1, но table.money == undefined.
После этого ответьте на вопрос, как быстрее искать glasses: 
обращением к pockets.glasses или head.glasses? Попробуйте протестировать.
*/
// В современных браузерах, с точки зрения производительности, нет разницы, 
//брать свойство из объекта или прототипа. 


/*
2. Свойство F.prototype и создание объектов через new
*/
/*
Прототип после создания

В примерах ниже создаётся объект new Rabbit, 
а затем проводятся различные действия с prototype.
Каковы будут результаты выполнения? Почему?
Начнём с этого кода. Что он выведет?
*/
function Rabbit() {}
Rabbit.prototype = {
  eats: true
};

var rabbit = new Rabbit();

alert( rabbit.eats );               // true

//Добавили строку (выделена), что будет теперь?
function Rabbit() {}
Rabbit.prototype = {
  eats: true
};
var rabbit = new Rabbit();
Rabbit.prototype = {};
alert( rabbit.eats );               // true
//А если код будет такой? (заменена одна строка):
function Rabbit(name) {}
Rabbit.prototype = {
  eats: true
};
var rabbit = new Rabbit();
Rabbit.prototype.eats = false;
alert( rabbit.eats );               // false
//А такой? (заменена одна строка)
function Rabbit(name) {}
Rabbit.prototype = {
  eats: true
};
var rabbit = new Rabbit();

delete rabbit.eats; // (*)

alert( rabbit.eats );               // true
//И последний вариант:
function Rabbit(name) {}
Rabbit.prototype = {
  eats: true
};
var rabbit = new Rabbit();
delete Rabbit.prototype.eats; // (*)
alert( rabbit.eats );               // undefined


/*
Аргументы по умолчанию

Есть функция Menu, которая получает аргументы в виде объекта options:
/* options содержит настройки меню: width, height и т.п. */
function Menu(options) {
  ...
}
/*Ряд опций должны иметь значение по умолчанию. Мы могли бы проставить их 
напрямую в объекте options:*/
function Menu(options) {
  options.width = options.width || 300; // по умолчанию ширина 300
  ...
}
/*…Но такие изменения могут привести к непредвиденным результатам, т.к. объект 
options может быть повторно использован во внешнем коде. Он передается в Menu 
для того, чтобы параметры из него читали, а не писали.
Один из способов безопасно назначить значения по умолчанию – скопировать все 
свойства options в локальные переменные и затем уже менять. Другой способ – 
клонировать options путём копирования всех свойств из него в новый объект, 
который уже изменяется.
При помощи наследования и Object.create предложите третий способ, который 
позволяет избежать копирования объекта и не требует новых переменных.
*/
function Menu(options) {
  options = Object.create(options);
  options.width = 300;

  alert("width: " + options.width); // возьмёт width из наследника
  alert("height: " + options.height); // возьмёт height из исходного объекта
}

var options = {
  width: 100,
  height: 200
};

var menu = new Menu(options);

alert("original width: " + options.width); // width исходного объекта
alert("original height: " + options.height); // height исходного объекта


/*
Есть ли разница между вызовами?

Создадим новый объект, вот такой:
*/
function Rabbit(name) {
  this.name = name;
}
Rabbit.prototype.sayHi = function() {
  alert( this.name );
};
var rabbit = new Rabbit("Rabbit");
//Одинаково ли сработают эти вызовы?
rabbit.sayHi();
Rabbit.prototype.sayHi();
Object.getPrototypeOf(rabbit).sayHi();
rabbit.__proto__.sayHi();
//Все ли они являются кросс-браузерными?
//Если нет – в каких браузерах сработает каждый?

/*Разница между вызовами
Первый вызов ставит this == rabbit, остальные ставят this равным 
Rabbit.prototype, следуя правилу "this – объект перед точкой".
Так что только первый вызов выведет Rabbit, в остальных он будет undefined.
Код для проверки:
*/
 function Rabbit(name) {
  this.name = name;
}
Rabbit.prototype.sayHi = function() {
  alert( this.name );
};

var rabbit = new Rabbit("Rabbit");

rabbit.sayHi();
Rabbit.prototype.sayHi();
Object.getPrototypeOf(rabbit).sayHi();
rabbit.__proto__.sayHi();
/*Совместимость
Первый вызов работает везде.
Второй вызов работает везде.
Третий вызов не будет работать в IE8-, там нет метода getPrototypeOf
Четвёртый вызов – самый «несовместимый», он не будет работать в IE10-, 
ввиду отсутствия свойства __proto__.
*/


/*
Создать объект тем же конструктором

Пусть у нас есть произвольный объект obj, созданный каким-то конструктором, 
каким – мы не знаем, но хотели бы создать новый объект с его помощью.
Сможем ли мы сделать так?
var obj2 = new obj.constructor();
Приведите пример конструкторов для obj, при которых такой код будет работать 
верно – и неверно.
*/
//Да, можем, но только если уверены, что кто-то позаботился о том, чтобы 
//значение constructor было верным.
//В частности, без вмешательства в прототип код точно работает, например:
 function User(name) {
  this.name = name;
}
var obj = new User('Вася');
var obj2 = new obj.constructor('Петя');
alert( obj2.name ); // Петя (сработало)
//Сработало, так как User.prototype.constructor == User.
//Но если кто-то, к примеру, перезапишет User.prototype и забудет указать 
//constructor, то такой фокус не пройдёт, например:
function User(name) {
  this.name = name;
}
User.prototype = {}; // (*)
var obj = new User('Вася');
var obj2 = new obj.constructor('Петя');
alert( obj2.name ); // undefined


/*
3. Встроенные "классы" в JavaScript
*/
/*
Добавить функциям defer

Добавьте всем функциям в прототип метод defer(ms), который откладывает вызов 
функции на ms миллисекунд.
После этого должен работать такой код:
*/
function f() {
  alert( "привет" );
}

f.defer(1000); // выведет "привет" через 1 секунду

Function.prototype.defer = function(ms) {
  setTimeout(this, ms);
}


/*
Добавить функциям defer с аргументами

Добавьте всем функциям в прототип метод defer(ms), который возвращает обёртку, 
откладывающую вызов функции на ms миллисекунд.
Например, должно работать так:
*/
function f(a, b) {
  alert( a + b );
}

//f.defer(1000)(1, 2); // выведет 3 через 1 секунду.
//То есть, должны корректно передаваться аргументы.
Function.prototype.defer = function(ms) {
  var f = this;
  return function() {
    var args = arguments,
    context = this;

    setTimeout(function() {
      f.apply(context, args);
    }, ms);
  }
}


/*
4. Свои классы на прототипах
*/
/*
Перепишите в виде класса

Есть класс CoffeeMachine, заданный в функциональном стиле.
Задача: переписать CoffeeMachine в виде класса с использованием прототипа.
Исходный код:
*/
function CoffeeMachine(power) {
  var waterAmount = 0;
  var WATER_HEAT_CAPACITY = 4200;
  function getTimeToBoil() {
    return waterAmount * WATER_HEAT_CAPACITY * 80 / power;
  }
  this.run = function() {
    setTimeout(function() {
      alert( 'Кофе готов!' );
    }, getTimeToBoil());
  };
  this.setWaterAmount = function(amount) {
    waterAmount = amount;
  };
}

var coffeeMachine = new CoffeeMachine(10000);
coffeeMachine.setWaterAmount(50);
coffeeMachine.run();
//P.S. При описании через прототипы локальные переменные недоступны методам, 
//поэтому нужно будет переделать их в защищённые свойства.
function CoffeeMachine(power) {
  // свойства конкретной кофеварки
  this._power = power;
  this._waterAmount = 0;
}

// свойства и методы для всех объектов класса
CoffeeMachine.prototype.WATER_HEAT_CAPACITY = 4200;

CoffeeMachine.prototype._getTimeToBoil = function() {
  return this._waterAmount * this.WATER_HEAT_CAPACITY * 80 / this._power;
};

CoffeeMachine.prototype.run = function() {
  setTimeout(function() {
    alert( 'Кофе готов!' );
  }, this._getTimeToBoil());
};

CoffeeMachine.prototype.setWaterAmount = function(amount) {
  this._waterAmount = amount;
};


/*
Хомяки с __proto__

Вы – руководитель команды, которая разрабатывает игру, хомяковую ферму. Один из 
программистов получил задание создать класс «хомяк» (англ – "Hamster").
Объекты-хомяки должны иметь массив food для хранения еды и метод found для 
добавления.
Ниже – его решение. При создании двух хомяков, если поел один – почему-то 
сытым становится и второй тоже.
В чём дело? Как поправить?
*/
function Hamster() {}
Hamster.prototype.food = []; // пустой "живот"
Hamster.prototype.found = function(something) {
  this.food.push(something);
};

// РЕШЕНИЕ
function Hamster() {
  this.food = [];
}
//Hamster.prototype.food = []; // пустой "живот"
Hamster.prototype.found = function(something) {
  this.food.push(something);
};

// Создаём двух хомяков и кормим первого
var speedy = new Hamster();
var lazy = new Hamster();
speedy.found("яблоко");
speedy.found("орех");
alert( speedy.food.length ); // 2
alert( lazy.food.length ); // 2 (!??)


/*
5. Наследование классов в JavaScript
*/
/*Найдите ошибку в наследовании

Найдите ошибку в прототипном наследовании. К чему она приведёт?
*/
function Animal(name) {
  this.name = name;
}
Animal.prototype.walk = function() {
  alert( "ходит " + this.name );
};
function Rabbit(name) {
  this.name = name;
}
Rabbit.prototype = Animal.prototype;
Rabbit.prototype.walk = function() {
  alert( "прыгает! и ходит: " + this.name );
};
Ошибка в строке:

//Rabbit.prototype = Animal.prototype;
//Эта ошибка приведёт к тому, что Rabbit.prototype и Animal.prototype – один и 
//тот же объект. В результате методы Rabbit будут помещены в него и, при 
//совпадении, перезапишут методы Animal.

//Правильный вариант этой строки:
Rabbit.prototype = Object.create(Animal.prototype);


/*
В чём ошибка в наследовании

Найдите ошибку в прототипном наследовании. К чему она приведёт?
*/
function Animal(name) {
  this.name = name;
  this.walk = function() {
    alert( "ходит " + this.name );
  };
}

function Rabbit(name) {
  Animal.apply(this, arguments);
}
Rabbit.prototype = Object.create(Animal.prototype);
Rabbit.prototype.walk = function() {
  alert( "прыгает " + this.name );
};
var rabbit = new Rabbit("Кроль");
rabbit.walk();

//Ошибка – в том, что метод walk присваивается в конструкторе Animal самому 
//объекту вместо прототипа.


/*
Класс "часы"

Есть реализация часиков, оформленная в виде одной функции-конструктора. У неё 
есть приватные свойства timer, template и метод render.
Задача: переписать часы на прототипах. Приватные свойства и методы сделать 
защищёнными.
P.S. Часики тикают в браузерной консоли (надо открыть её, чтобы увидеть).
*/
function Clock(options) {

  var template = options.template;
  var timer;

  function render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var min = date.getMinutes();
    if (min < 10) min = '0' + min;

    var sec = date.getSeconds();
    if (sec < 10) sec = '0' + sec;

    var output = template.replace('h', hours)
                           .replace('m', min)
                           .replace('s', sec);

    console.log(output);
  }

  this.stop = function() {
    clearInterval(timer);
  };

  this.start = function() {
    render();
    timer = setInterval(render, 1000);
  }

}
//РЕШЕНИЕ
function Clock(options) {
  this._template = options.template;
}

Clock.prototype._render = function render() {
  var date = new Date();

  var hours = date.getHours();
  if (hours < 10) hours = '0' + hours;

  var min = date.getMinutes();
  if (min < 10) min = '0' + min;

  var sec = date.getSeconds();
  if (sec < 10) sec = '0' + sec;

  var output = this._template.replace('h', hours)
                               .replace('m', min)
                               .replace('s', sec);

  console.log(output);
};

Clock.prototype.stop = function() {
  clearInterval(this._timer);
};

Clock.prototype.start = function() {
  this._render();
  var self = this;
  this._timer = setInterval(function() {
    self._render();
  }, 1000);
};


/*
Класс "расширенные часы"

Есть реализация часиков на прототипах. Создайте класс, расширяющий её, 
добавляющий поддержку параметра precision, который будет задавать частоту тика 
в setInterval. Значение по умолчанию: 1000.
Для этого класс Clock надо унаследовать. Пишите ваш новый код в файле 
extended-clock.js.
Исходный класс Clock менять нельзя.
Пусть конструктор потомка вызывает конструктор родителя. Это позволит избежать 
проблем при расширении Clock новыми опциями.
P.S. Часики тикают в браузерной консоли (надо открыть её, чтобы увидеть).
*/
//clock.js
function Clock(options) {
  this._template = options.template;
}

Clock.prototype._render = function render() {
  var date = new Date();

  var hours = date.getHours();
  if (hours < 10) hours = '0' + hours;

  var min = date.getMinutes();
  if (min < 10) min = '0' + min;

  var sec = date.getSeconds();
  if (sec < 10) sec = '0' + sec;

  var output = this._template.replace('h', hours)
  .replace('m', min)
  .replace('s', sec);

  console.log(output);
};

Clock.prototype.stop = function() {
  clearInterval(this._timer);
};

Clock.prototype.start = function() {
  this._render();
  var self = this;
  this._timer = setInterval(function() {
    self._render();
  }, 1000);
};

//extended-clock.js
/*
function extend(Child, Parent) {
  Child.prototype = inherit(Parent.prototype);
  Child.prototype.constructor = Child;
  Child.parent = Parent.prototype;
}

function inherit(proto) {
  function F() {}
  F.prototype = proto;
  return new F;
}
*/
// ваш код
function ExtendedClock(options) {
  Clock.apply(this, arguments);
  this._precision = +options.precision || 1000;
}

ExtendedClock.prototype = Object.create(Clock.prototype);

ExtendedClock.prototype.start = function() {
  this._render();
  var self = this;
  this._timer = setInterval(function() {
    self._render();
  }, this._precision);
};


/*
Меню с таймером для анимации

Есть класс Menu. У него может быть два состояния: открыто STATE_OPEN и закрыто 
STATE_CLOSED.
Создайте наследника AnimatingMenu, который добавляет третье состояние 
STATE_ANIMATING.
При вызове open() состояние меняется на STATE_ANIMATING, а через 1 секунду, по 
таймеру, открытие завершается вызовом open() родителя.
Вызов close() при необходимости отменяет таймер анимации (назначаемый в open) и 
передаёт вызов родительскому close.
Метод showState для нового состояния выводит "анимация", для остальных – 
полагается на родителя.
*/
function Menu(state) {
  this._state = state || Menu.STATE_CLOSED;
};

Menu.STATE_OPEN = 1;
Menu.STATE_CLOSED = 0;

Menu.prototype.open = function() {
  this._state = Menu.STATE_OPEN;
};

Menu.prototype.close = function() {
  this._state = Menu.STATE_CLOSED;
};

Menu.prototype._stateAsString = function() {
  switch (this._state) {
    case Menu.STATE_OPEN:
      return 'открыто';

    case Menu.STATE_CLOSED:
      return 'закрыто';
  }
};

Menu.prototype.showState = function() {
  alert(this._stateAsString());
};

//РЕШЕНИЕ
function AnimatingMenu() {
    Menu.apply(this, arguments);
  }

AnimatingMenu.prototype = Object.create(Menu.prototype);
AnimatingMenu.prototype.STATE_ANIMATING = 2;

AnimatingMenu.prototype.open = function() {
  var self = this;

  this._state = this.STATE_ANIMATING;

  this._timer = setTimeout(function() {
    Menu.prototype.open.call(self);
  }, 1000);
};

AnimatingMenu.prototype.close = function() {
  clearTimeout(this._timer);
  Menu.prototype.close.apply(this);
};

AnimatingMenu.prototype._stateAsString = function() {
  switch (this._state) {
    case this.STATE_ANIMATING:
      return 'анимация';

        default:
          return Menu.prototype._stateAsString.call(this);
      }
    };


/*
Что содержит constructor?

В коде ниже создаётся простейшая иерархия классов: Animal -> Rabbit.
Что содержит свойство rabbit.constructor? Распознает ли проверка в alert объект 
как Rabbit?
*/
function Animal() {}
function Rabbit() {}
Rabbit.prototype = Object.create(Animal.prototype);
var rabbit = new Rabbit();
alert( rabbit.constructor == Rabbit ); // что выведет? false

alert( rabbit.constructor == Animal ); // true


/*
6. Проверка класса: "instanceof"
*/
/*
Странное поведение instanceof

Почему instanceof в коде ниже возвращает true, ведь объект a явно создан не B()?
*/
function A() {}
function B() {}
A.prototype = B.prototype = {};
var a = new A();
alert( a instanceof B ); // true
//a.__proto__ == B.prototype, поэтому instanceof возвращает true


/*
Что выведет instanceof?

В коде ниже создаётся простейшая иерархия классов: Animal -> Rabbit.
Что выведет instanceof?
Распознает ли он rabbit как Animal, Rabbit и к тому же Object?
*/
function Animal() {}
function Rabbit() {}
Rabbit.prototype = Object.create(Animal.prototype);
var rabbit = new Rabbit();
alert( rabbit instanceof Rabbit );          // true
alert( rabbit instanceof Animal );          // true
alert( rabbit instanceof Object );           // true


/*
7. Свои ошибки, наследование от Error
*/
/*
Унаследуйте от SyntaxError

Создайте ошибку FormatError, которая будет наследовать от встроенного класса 
SyntaxError.
Синтаксис для её создания – такой же, как обычно:
*/
var err = new FormatError("ошибка форматирования");

alert( err.message ); // ошибка форматирования
alert( err.name ); // FormatError
alert( err.stack ); // стек на момент генерации ошибки

alert( err instanceof SyntaxError ); // true

//РЕШЕНИЕ
function FormatError(message) {
  this.name = "FormatError";

  this.message = message;

  if (Error.captureStackTrace) {
    Error.captureStackTrace(this, this.constructor);
  } else {
    this.stack = (new Error()).stack;
  }

}

FormatError.prototype = Object.create(SyntaxError.prototype);
FormatError.prototype.constructor = FormatError;


/*
8. Примеси
*/

